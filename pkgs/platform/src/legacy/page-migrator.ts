import { traverse } from '@babel/core'
import generate from '@babel/generator'
import { parse } from '@babel/parser'
import { log } from 'boot'
import { pathExists, readdir, readFile, writeFile } from 'fs-extra'
import { ellapsedTime } from 'libs'
import trim from 'lodash.trim'
import { join } from 'path'
import { askFigmaBase } from '../dev/dev-hmr'
import { CustomGlobal } from '../server'
import { API, baseDir, initBase, oldCmsDir, Page } from '../system/init'
declare const global: CustomGlobal

export const upgradePage = async (page: any) => {
  const existingHtmlPath = join(oldCmsDir.templates, `${page.id}.html`)
  let existingHtml = ''
  if (await pathExists(existingHtmlPath)) {
    existingHtml = await readFile(existingHtmlPath, 'utf-8')
  }

  const { html, effect } = await parseFigmaJSX(existingHtml)

  // request html for current page_id (id)
  let figmaOutput = (await askFigmaBase(
    async (x) => {
      let frame = x.figma.cache.framesByTargetId[x.id]
      const babel = x.window.babel

      if (frame && frame.target) {
        x.figma.current.nodeIsLoading = true
        x.figma.current.nextNodeName = frame.target.name
        x.figma.main.render()

        if (babel.prettier) {
          x.figma.current.node = frame
          x.figma.current.nodeIsLoading = true
          x.figma.main.render()

          if (!frame.html) {
            const frameTree = await x.figma.tools.fetchFrame(frame.id, false)
            x.figma.cache.pages[frameTree.page.id].frames[frameTree.id] =
              frameTree
            x.figma.cache.framesByTargetId[x.id] = frameTree
            frame = frameTree
          }

          setTimeout(() => {
            x.figma.current.nodeIsLoading = false
            x.figma.main.render()
          })

          x.figma.current.nodeIsLoading = false
          x.figma.main.render()

          return {
            nodes: await x.figma.tools.fetchNodeMap(frame.id),
            source: babel.prettier(
              `<>${frame.html.effect}${x.figma.tools.recursivePrintHtml(
                frame
              )}</>`
            ),
          }
        }
      } else {
        // frame does not exist in figma, let's create it
        const node_id = (await x.askFigma(
          (f) => {
            const frame = figma.createFrame()
            frame.name = `${f.id} | ${f.name.substr(0, 30)}`
            frame.setPluginData(
              'target',
              JSON.stringify({
                type: 'page',
                id: f.id,
                name: f.name,
              })
            )
            frame.setPluginData('effect', f.effect)
            frame.setPluginData('wrapCode', f.html)
            figma.root.children[0].appendChild(frame)

            return frame.id
          },
          {
            name: x.name,
            id: x.id,
            html: x.existing.html,
            effect: x.existing.effect,
          }
        )) as string

        x.figma.current.nodeIsLoading = false
        x.figma.main.render()

        return {
          nodes: await x.figma.tools.fetchNodeMap(node_id),
          source: `<>${x.existing.effect}${x.existing.html}</>`,
        }
      }
    },
    { id: page.id, existing: { html, effect }, name: page.title }
  )) as { nodes: Record<string, any>; source: string }

  if (figmaOutput) {
    global.dev.history.pushChange(
      'page',
      page.id,
      figmaOutput.source,
      JSON.stringify(figmaOutput.nodes)
    )

    await writeFile(join(baseDir.page, `${page.id}.jsx`), figmaOutput.source)

    if (page.content.server_on_load) {
      await writeFile(
        join(baseDir.page, page.id + '_api.ts'),
        page.content.server_on_load
      )
    }

    const str = JSON.stringify(
      {
        id: page.id,
        name: page.title,
        layout_id: page.layout_id || page.parent_id,
        url: page.slug,
      } as Page,
      null,
      2
    )
    await writeFile(join(baseDir.page, `${page.id}.json`), str)
  } else {
    log(
      'migrate',
      `Failed to migrate ${existingHtmlPath}:  ${JSON.stringify(page, null, 2)}`
    )
  }
}

export const upgradeLayout = async (page: any) => {
  const existingHtmlPath = join(oldCmsDir.templates, `${page.id}.html`)
  let existingHtml = ''
  if (await pathExists(existingHtmlPath)) {
    existingHtml = await readFile(existingHtmlPath, 'utf-8')
  }

  const { html, effect, fre } = await parseFigmaJSX(existingHtml)

  // request html for current page_id (id)
  let figmaOutput = (await askFigmaBase(
    async (x) => {
      await x.askFigma(() => (figma.currentPage.selection = []))

      x.figma.current.nodeIsLoading = true
      x.figma.current.nextNodeName = x.name
      x.figma.main.render()

      let componentPageId = (await x.askFigma(() => {
        for (let page of figma.root.children) {
          if (page.name.toLowerCase() === 'components') {
            return page.id
          }
        }

        // page is not found, lets create it
        const newPage = figma.createPage()
        newPage.name = 'Components'
        figma.root.appendChild(newPage)
        return newPage.id
      })) as string

      let frame = x.figma.cache.framesByTargetId[x.id]
      const babel = x.window.babel

      if (babel.prettier) {
        if (frame) {
          x.figma.current.node = frame
          x.figma.current.nodeIsLoading = true
          x.figma.main.render()

          if (!frame.html) {
            const frameTree = await x.figma.tools.fetchFrame(frame.id, false)
            x.figma.cache.pages[frameTree.page.id].frames[frameTree.id] =
              frameTree
            x.figma.cache.framesByTargetId[x.id] = frameTree
            frame = frameTree
          }

          setTimeout(() => {
            x.figma.current.nodeIsLoading = false
            x.figma.main.render()
          })

          x.figma.current.nodeIsLoading = false
          x.figma.main.render()
          return {
            nodes: await x.figma.tools.fetchNodeMap(frame.id),
            source: babel.prettier(
              `<>${frame.html.effect}${x.figma.tools.recursivePrintHtml(
                frame
              )}</>`
            ),
          }
        }
        // frame does not exist in figma cache
        // it means this frame is a layout
        const node_id = (await x.askFigma(
          (f) => {
            const layout = figma.root.findOne((e) => {
              if (e.name.startsWith('layout:')) {
                const name = e.name.substr('layout:'.length).trim()
                if (name.toLowerCase() === f.name) {
                  return true
                }
              }
              return false
            })

            const frame = layout || figma.createFrame()
            frame.name = `layout: ${f.name.substr(0, 30)}`
            frame.setPluginData(
              'target',
              JSON.stringify({
                type: 'page',
                id: f.id,
                name: f.name,
              })
            )
            frame.setPluginData('effect', f.effect)
            frame.setPluginData('f-html', f.html)
            const page = figma.getNodeById(f.page_id) as PageNode
            if (page) {
              page.appendChild(frame as any)
            }
            return frame.id
          },
          {
            name: x.name,
            id: x.id,
            page_id: componentPageId,
            html: x.existing.html,
            effect: x.existing.effect,
          }
        )) as string

        x.figma.current.nodeIsLoading = false
        x.figma.main.render()
        return {
          nodes: await x.figma.tools.fetchNodeMap(node_id),
          source: `<>${x.existing.effect}${x.existing.html}</>`,
        }
      }
    },
    { id: page.id, existing: { html, effect }, name: page.title }
  )) as { nodes: Record<string, any>; source: string }

  if (figmaOutput) {
    // TODO: create FRE is does not exists in figma

    // store changed source in dev history
    global.dev.history.pushChange(
      'page',
      page.id,
      figmaOutput.source,
      JSON.stringify(figmaOutput.nodes)
    )

    await writeFile(join(baseDir.layout, `${page.id}.jsx`), figmaOutput.source)

    if (page.content.server_on_load) {
      await writeFile(
        join(baseDir.layout, page.id + '_api.ts'),
        page.content.server_on_load
      )
    }

    const str = JSON.stringify(
      {
        id: page.id,
        name: page.title,
      } as Page,
      null,
      2
    )

    await writeFile(join(baseDir.layout, `${page.id}.json`), str)
  }
}

export const upgradeAPI = async (page: any) => {
  if (page.content.server_on_load) {
    await writeFile(
      join(baseDir.api, page.id + '_api.ts'),
      page.content.server_on_load
    )
  }

  const str = JSON.stringify(
    {
      id: page.id,
      name: page.title,
      type: 'API',
      url: page.slug,
      serverOnLoad: () => {},
    } as API,
    null,
    2
  )

  await writeFile(join(baseDir.api, `${page.id}.json`), str)
}

export const startMigration = async () => {
  if (global.baseUpgrade.migrating) {
    return
  }
  global.baseUpgrade.migrating = true
  const ms = new Date().getTime()
  log('base', '')
  log('base', '=== Migration ===')
  const list = (await readdir(oldCmsDir.templates)).filter((e) =>
    e.endsWith('.json')
  )
  log('base', `Analyzing ${list.length} existing files `, false)

  const pendingUpgrade = {
    page: [] as any[],
    layout: [] as any[],
    api: [] as any[],
  }

  let listCount = 0
  for (let i of list) {
    const pagePath = join(oldCmsDir.templates, i)
    if (pagePath.endsWith('.json')) {
      const page = JSON.parse(await readFile(pagePath, 'utf-8'))
      if (page.content.type === 'Page') {
        pendingUpgrade.page.push(page)
      } else if (page.content.type === 'Layout') {
        pendingUpgrade.layout.push(page)
      } else if (page.content.type === 'API') {
        pendingUpgrade.api.push(page)
      }

      if (listCount++ % 20 === 0) {
        process.stdout.write('.')
      }
    }
  }

  process.stdout.write(' OK \n')

  log('migrate', `Upgrading ${pendingUpgrade.layout.length} layout `, false)
  listCount = 0
  for (let layout of pendingUpgrade.layout) {
    await upgradeLayout(layout)

    if (listCount++ % 20 === 0) {
      process.stdout.write('.')
    }
  }
  process.stdout.write(' OK \n')

  log('migrate', `Upgrading ${pendingUpgrade.page.length} pages `, false)
  listCount = 0
  for (let page of pendingUpgrade.page) {
    await upgradePage(page)

    if (listCount++ % 20 === 0) {
      process.stdout.write('.')
    }
  }
  process.stdout.write(' OK \n')

  log('migrate', `Upgrading ${pendingUpgrade.api.length} APIs `, false)
  listCount = 0
  for (let page of pendingUpgrade.api) {
    await upgradeAPI(page)

    if (listCount++ % 20 === 0) {
      process.stdout.write('.')
    }
  }
  process.stdout.write(' OK \n')

  log('migrate', 'Removing old files')
  // await remove(oldCmsDir.cms)

  await initBase()

  log('platform', '')

  const port = global.port
  log(
    'platform',
    `Migrated in [${ellapsedTime(ms)}s] http://localhost${
      port === 80 || port === 443 ? `` : `:${port}`
    }`
  )
  global.baseUpgrade.migrating = false
}

const parseFigmaJSX = async (existing: string) => {
  const parsed = parse(existing, {
    sourceType: 'module',
    plugins: ['jsx', 'typescript'],
  })

  let effect = ''
  let html = ''

  traverse(parsed, {
    enter: (path) => {
      const c = path.node
      if (
        c.type === 'JSXElement' &&
        c.openingElement.name.type === 'JSXIdentifier' &&
        c.start &&
        c.end
      ) {
        if (c.openingElement.name.name === 'effect') {
          effect = existing.substr(c.start, c.end - c.start)
          path.remove()
        }
        if (c.openingElement.name.name === 'figma-react-elements') {
          path.remove()
        }
      }
    },
  })

  html = trim(generate(parsed, {}).code, ';')
  if (html.startsWith('<>') && html.endsWith('</>')) {
    html = html.substr('<>'.length, html.length - '<></>'.length)
  }

  return {
    html,
    effect,
    fre: {},
  }
}
