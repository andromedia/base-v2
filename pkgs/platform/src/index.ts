import { dirs } from 'boot'
import { expose } from 'builder'
import { ellapsedTime, waitUntil } from 'libs'
import fetch, { Headers } from 'node-fetch'
import { join } from 'path'
import zlib from 'zlib'
import { broadcastHMR } from './dev/dev-hmr'
import { CustomGlobal, MainControl, server } from './server'
import { applyDevBuild, compressPublic } from './system/init'
declare const global: CustomGlobal

require('dotenv').config({ path: join(dirs.root, '.env') })
global.fetch = fetch
global.Headers = Headers

let main: MainControl = {
  signal: async () => {},
  notifyDevBuild: async (path: string) => {
    return ''
  },
  onMessage: async () => {},
}

export const devBuild = {
  tstamp: 0,
  count: 0,
  processing: true,
  files: {} as Record<string, { raw: Buffer; gz: Buffer; br: Buffer }>,
}
expose({
  start: async (parent, args) => {
    main.signal = async (module, data) => {
      parent.sendTo('main', {
        type: 'platform-signal',
        module,
        data,
      })
    }
    main.notifyDevBuild = async (path: string) => {
      parent.notify({ action: 'dev-build', type: 'platform-signal', path })
      return ''
    }
    args.devBuild = devBuild

    if (args.mode === 'dev') {
      await waitUntil(() => !devBuild.processing)
      if (global.shouldExit) return
    }

    await server(main, args, parent)
  },
  onMessage: async (raw: any) => {
    if (typeof raw === 'string') {
      if (raw === 'notify-refresh-all') {
        broadcastHMR({ type: 'hmr-pending-reload-all' })
        return
      }
      if (raw === 'exit') {
        global.shouldExit = true
      }

      const type = raw.substr(0, 5)
      const msg = raw.substr(5)

      if (type === 'devb!') {
        const len = parseInt(msg.substr(0, 4))
        if (len === 0) {
          devBuild.tstamp = parseInt(msg.substr(4))
        } else if (len === 9999) {
          if (!global.cache) {
            await Promise.all(
              Object.entries(devBuild.files).map(async ([path, content]) => {
                if (global.shouldExit) return

                if (
                  path.endsWith('.js') ||
                  path.endsWith('.css') ||
                  path.endsWith('.svg')
                ) {
                  const br = new Promise<void>((resolve) => {
                    zlib.brotliCompress(content.raw, {}, (_, result) => {
                      content.br = result
                      resolve()
                    })
                  })

                  const gz = new Promise<void>((resolve) => {
                    zlib.gzip(content.raw, {}, (_, result) => {
                      content.gz = result
                      resolve()
                    })
                  })

                  await Promise.all([br, gz])
                  if (devBuild.count++ % 50 === 0) process.stdout.write('.')
                }
              })
            )
            devBuild.processing = false
          }
          if (!!global.cache) {
            process.stdout.write('.')
            await applyDevBuild()
            process.stdout.write('.')
            broadcastHMR({ type: 'hmr-reload-all' })
            compressPublic(global.cache)
          }

          process.stdout.write(` ${ellapsedTime(devBuild.tstamp)}s [DONE]\n`)
        } else {
          const path = msg.substr(4, len)
          const content = Buffer.from(msg.substr(4 + len), 'utf-8')
          const source = { raw: content, br: null as any, gz: null as any }

          devBuild.files[
            path.substr(join(dirs.app.web, 'build', 'web').length)
          ] = source
          if (devBuild.count++ % 50 === 0) process.stdout.write('.')
        }
      }

      if (global.shouldExit && raw !== 'exit') return
    }

    await main.onMessage(raw)
  },
})
