import bsql from 'better-sqlite3'
import { dirs } from 'boot'
import { ensureDirSync, pathExistsSync } from 'fs-extra'
import sortBy from 'lodash.sortby'
import { dirname, join } from 'path'
import zlib from 'zlib'

type PageType = 'page' | 'api' | 'layout'
type PageHistory = {
  dtype: PageType
  id: string
  history: PageHistorySingle[]
}
type PageHistorySingle = {
  jsx: string
  tstamp: number
  nodes: string
}
const hash = (data: string) => {
  return require('crypto').createHash('sha1').update(data).digest('base64')
}
export class BaseHistory {
  private db: bsql.Database
  private page: Record<string, Omit<PageHistory, 'dtype'>> = {}
  private api: Record<string, Omit<PageHistory, 'dtype'>> = {}
  private layout: Record<string, Omit<PageHistory, 'dtype'>> = {}

  async pushChange(
    type: PageType,
    id: string,
    rawjsx: string,
    rawnodes: string
  ): Promise<boolean> {
    const content = this[type][id]
    let jsx,
      nodes = ''

    await Promise.all([
      new Promise<void>((resolve) => {
        zlib.brotliCompress(Buffer.from(rawjsx || ''), {}, (_, result) => {
          jsx = result.toString('utf-8')
          resolve()
        })
      }),
      new Promise<void>((resolve) => {
        zlib.brotliCompress(Buffer.from(rawnodes || ''), {}, (_, result) => {
          nodes = result.toString('utf-8')
          resolve()
        })
      }),
    ])


    if (content && content.history && content.history.length > 0) {
      const lastPatch = content.history[content.history.length - 1]
      if (nodes && jsx && lastPatch.nodes === nodes && lastPatch.jsx === jsx) {
        // no changes were made
        return false
      }

      const data: PageHistorySingle = {
        jsx: jsx,
        nodes: nodes,
        tstamp: new Date().getTime(),
      }
      content.history.push(data)
      this.set({ ...data, id, type })
      return true
    }

    // no-previous patches, store the jsx and hash
    const data = {
      jsx: jsx,
      nodes: nodes,
      tstamp: new Date().getTime(),
    }
    this[type][id] = {
      id,
      history: [data],
    }
    this.set({ ...data, id, type })
    return true
  }

  listChanges(type: PageType, id: string): { tstamp: number; md5: string }[] {
    // return list of change for this page id
    return []
  }

  async getCode(type: PageType, id: string, tstamp: number): Promise<string> {
    // return code that has been changes applied
    return ''
  }

  private set(data: PageHistorySingle & { id: string; type: PageType }) {
    try {
      const stmt = this.db.prepare(`
    INSERT INTO data(dtype,id,tstamp,nodes,jsx) VALUES(?,?,?,?,?);
  `)
      stmt.run(data.type, data.id, data.tstamp, data.nodes, data.jsx)
    } catch (e) {
      console.log(e)
    }
  }

  constructor() {
    const path = join(dirs.app.web, 'src', 'base', 'base.history')
    if (!pathExistsSync(dirname(path))) {
      ensureDirSync(dirname(path))
    }
    this.db = new bsql(path)
    this.db.exec(`
CREATE TABLE IF NOT EXISTS data (
  dtype TEXT,
  id TEXT,
  tstamp INTEGER,
  jsx TEXT,
  nodes TEXT,

  PRIMARY KEY (dtype, id, tstamp)
);
`)
    const res = this.db.prepare('select * from data').all()
    for (let row of res) {
      const page = this[row.dtype]
      if (!page[row.id]) {
        page[row.id] = { id: row.id, patches: [] }
      }
      const cur = page[row.id]
      cur.patches.push({
        nodes: row.patch,
        jsx: row.jsx,
        tstamp: row.tstamp,
      } as PageHistorySingle)
      cur.patches = sortBy(cur.patches, 'tstamp')
    }
  }
}
