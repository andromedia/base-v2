import { dirs, log } from 'boot'
import { readFile } from 'fs-extra'
import { ellapsedTime } from 'libs'
import { join } from 'path'
import { overrideWebIndex } from '../../../../main/src/utils/overrideWebIndex'
import { CustomGlobal } from '../../server'
import { broadcastHMR } from '../dev-hmr'
declare const global: CustomGlobal

export const saveSyncIndex = async () => {
  const ms = new Date().getTime()
  log('refresh', `🏗️  app/public/index.html `, false)
  await overrideWebIndex('dev')

  global.cache.index = await readFile(
    join(dirs.app.web, 'build', 'web', 'index.html'),
    'utf-8'
  )
  global.cache.index = global.cache.index.replace(
    `</body>`,
    `<script type="module" src="${global.mainjs}"></script>`
  )

  broadcastHMR({ type: 'hmr-reload-all' })

  const time = ellapsedTime(ms)
  process.stdout.write(`${time === 0 ? '' : `${time}s `}[DONE]\n`)
  return
}
