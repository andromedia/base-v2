import { dirs } from 'boot'
import { Watcher } from 'builder'
import { pathExists } from 'fs-extra'
import { basename, join } from 'path'
import { CustomGlobal } from '../../server'
import { saveSyncAPI } from './save-sync-api'
import { saveSyncIndex } from './save-sync-index'
import { saveSyncLayout } from './save-sync-layout'
import { createPage, saveSyncPage, saveSyncPageJson } from './save-sync-page'
declare const global: CustomGlobal

export const startSaveSync = () => {
  return new Watcher(
    [
      join(dirs.app.web, 'public', 'index.html'),
      join(dirs.app.web, 'src', 'base', 'page'),
      join(dirs.app.web, 'src', 'base', 'api'),
      join(dirs.app.web, 'src', 'base', 'layout'),
    ],
    async (type, file) => {
      try {
        let id = basename(file).split('.').shift()
        let ext = basename(file).split('.').pop()
        if (file.indexOf('index.html') >= 0) {
          saveSyncIndex()
          return
        }

        if (id && (await pathExists(file))) {
          let isAPI = false

          if (id.endsWith('_api')) {
            isAPI = true
            id = id.substr(0, id.length - 4)
          }
          if (id.length > 5) {
            id = id.substr(0, 5)
          }

          const page = global.cache.page[id]
          if (type === 'add') {
            if (file.startsWith(join(dirs.app.web, 'src', 'base', 'page'))) {
              createPage(file)
              return
            }
          } else if (page) {
            if (!isAPI) {
              if (ext === 'json') {
                saveSyncPageJson(page)
              } else {
                saveSyncPage(id, page)
              }
            } else {
              saveSyncAPI('page', id)
            }
          }

          const layout = global.cache.layout[id]
          if (layout) {
            if (!isAPI) {
              saveSyncLayout(id, layout)
            } else {
              saveSyncAPI('layout', id)
            }
          }

          const api = global.cache.api[id]
          if (api) {
            saveSyncAPI('api', id)
          }
        }
      } catch (e) {}
    }
  )
}
