import { traverse } from '@babel/core'
import generate from '@babel/generator'
import { parse } from '@babel/parser'
import { dirs, log } from 'boot'
import {
  pathExists,
  readFile,
  readJSON,
  rename,
  writeFile,
  writeJSON,
} from 'fs-extra'
import { ellapsedTime } from 'libs'
import { snakeCase } from 'lodash'
import deepClone from 'lodash.clonedeep'
import { basename, join } from 'path'
import { CustomGlobal } from '../../../src/server'
import { Page, setupInitDataFromCache } from '../../system/init'
import { pageJSXNameMap, reloadSinglePage } from '../../system/page/page-loader'
import { askFigmaBase, broadcastHMR } from '../dev-hmr'
declare const global: CustomGlobal
const pagePath = join(dirs.app.web, 'src', 'base', 'page')

export const createPage = async (oldFileName: string) => {
  if (oldFileName.endsWith('.json')) {
    let id = basename(oldFileName).split('.').shift() || ''
    const firstPage = global.cache.page[Object.keys(global.cache.page)[0]]
    await writeJSON(
      oldFileName,
      {
        id,
        name: 'new-page',
        layout_id: firstPage.layout_id,
        url: '/',
      },
      { spaces: 2 }
    )

    await writeFile(
      join(pagePath, `${id}-${'new-page'}.jsx`),
      `;<>
  <effect meta={{}} run={async () => {}} />
  <div></div>
</>`
    )
    pageJSXNameMap[id] = 'new-page'
    await reloadSinglePage(id, pagePath, global.cache)
  }
}

export const saveSyncPageJson = async (page: Page) => {
  const jsonPath = join(pagePath, `${page.id}.json`)
  const newPage = (await readJSON(jsonPath)) as Page
  const newPageName = '-' + snakeCase(newPage.name).replace(/_/gi, '-')
  const existingPageName = pageJSXNameMap[page.id]
  if (existingPageName !== newPageName) {
    if (await pathExists(join(pagePath, `${page.id}${existingPageName}.jsx`))) {
      await rename(
        join(pagePath, `${page.id}${existingPageName}.jsx`),
        join(pagePath, `${page.id}${newPageName}.jsx`)
      )
      pageJSXNameMap[page.id] = newPageName
    }
  }

  if (!existingPageName) {
    await writeFile(
      join(pagePath, `${page.id}${newPageName}.jsx`),
      `;<>
  <effect meta={{}} run={async () => {}} />
  <div></div>
</>`
    )
    pageJSXNameMap[page.id] = newPageName
  }

  if (newPage.id !== page.id) {
    newPage.id = page.id
    await writeJSON(jsonPath, newPage, {
      spaces: 2,
    })
  }

  if (newPage.url !== page.url) {
    await reloadSinglePage(page.id, pagePath, global.cache)
    await setupInitDataFromCache()
    log(
      'refresh',
      `🏗️  app/web/base/page/${page.id}.json url -> ${newPage.url}`
    )
  }
}

export const saveSyncPage = async (id: string, page: Page) => {
  const ms = new Date().getTime()

  const pageName = pageJSXNameMap[id]
  if (pageName) {
    log('refresh', `🏗️  app/web/src/base/page/${id}${pageName}.jsx `, false)

    await reloadSinglePage(
      `${id}.json`,
      join(dirs.app.web, 'src', 'base', 'page'),
      global.cache
    )

    const file = await readFile(
      join(dirs.app.web, 'src', 'base', 'page', `${id}${pageName}.jsx`),
      'utf-8'
    )
    const time = ellapsedTime(ms)
    process.stdout.write(`${time === 0 ? '' : `${time}s `}[DONE]\n`)

    broadcastHMR({ type: 'hmr-reload-app' })
    let nodes = ''

    const saving = global.figma.saving[page.id]
    if (saving) {
      nodes = JSON.stringify(saving.nodes)
      delete global.figma.saving[page.id]
    } else {
      // apply all nodes to figma
      // parse and traverse while applying all fnode to figma
      const parsed = parse(file, {
        sourceType: 'module',
        plugins: ['jsx', 'typescript'],
      })

      traverse(parsed, {
        enter: (path) => {
          const c = path.node
          if (
            c.type === 'JSXElement' &&
            c.openingElement.name.type === 'JSXIdentifier' &&
            c.start &&
            c.end
          ) {
            if (c.openingElement.name.name === 'fnode') {
              let node_id = ''
              for (let a of c.openingElement.attributes) {
                if (
                  a.type === 'JSXAttribute' &&
                  a.name.type === 'JSXIdentifier' &&
                  a.value &&
                  a.value.type === 'StringLiteral' &&
                  a.name.name === 'id'
                ) {
                  node_id = a.value.value
                }
              }

              if (node_id) {
                const start = c.children[0].start
                const end = c.children[c.children.length - 1].end

                if (start && end) {
                  let childrenGenerated = false
                  const childrens = deepClone(c.children)
                  traverse(
                    {
                      start: start,
                      end: end,
                      type: 'File',
                      program: {
                        sourceType: 'module',
                        start: start,
                        end: end,
                        type: 'Program',
                        body: childrens,
                      },
                    } as any,
                    {
                      enter: (path) => {
                        const c = path.node
                        if (c.type === 'JSXElement') {
                          if (
                            c.openingElement.name.type === 'JSXIdentifier' &&
                            c.openingElement.name.name === 'fnode'
                          ) {
                            let id = ''
                            for (let e of c.openingElement.attributes) {
                              if (
                                e.type === 'JSXAttribute' &&
                                e.name.name === 'id' &&
                                e.value?.type === 'StringLiteral'
                              ) {
                                id = e.value.value
                              }
                            }
                            if (id !== node_id) {
                              if (childrenGenerated) {
                                path.remove()
                              } else {
                                childrenGenerated = true
                                path.replaceWithSourceString(`{children}`)
                              }
                            }
                          }
                        }
                      },
                    }
                  )

                  const result: string[] = []
                  for (let i of childrens) {
                    result.push(generate(i).code)
                  }
                  askFigmaBase(
                    (x) => {
                      x.askFigma(
                        (p) => {
                          const node = figma.getNodeById(p.id)
                          if (node) {
                            node.setPluginData('f-html', p.code)
                          }
                        },
                        { id: x.id, code: x.code }
                      )
                    },
                    {
                      id: node_id,
                      code: result
                        .join('\n')
                        .replace(/\{\s+children\s+\}/, '{children}'),
                    }
                  )
                }
              }
            }
          }
        },
      })
      // and then fetch it again
      nodes = JSON.stringify(
        await askFigmaBase(
          (x) => {
            return x.figma.tools.fetchNodeMap(x.id)
          },
          { id }
        )
      )
    }

    await global.dev.history.pushChange('page', page.id, file, nodes)
  }
}
