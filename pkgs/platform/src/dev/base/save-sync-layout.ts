import { traverse } from '@babel/core'
import generate from '@babel/generator'
import { parse } from '@babel/parser'
import { dirs, log } from 'boot'
import { readFile } from 'fs-extra'
import { ellapsedTime } from 'libs'
import deepClone from 'lodash.clonedeep'
import { join } from 'path'
import { CustomGlobal } from '../../server'
import { Layout } from '../../system/init'
import { reloadSingleLayout } from '../../system/page/layout-loader'
import { askFigmaBase, broadcastHMR } from '../dev-hmr'
declare const global: CustomGlobal

export const saveSyncLayout = async (id: string, layout: Layout) => {
  const ms = new Date().getTime()
  log('refresh', `🏗️  app/web/src/base/layout/${id}.jsx `, false)

  await reloadSingleLayout(
    `${id}.json`,
    join(dirs.app.web, 'src', 'base', 'layout'),
    global.cache
  )

  const file = await readFile(
    join(dirs.app.web, 'src', 'base', 'layout', `${id}.jsx`),
    'utf-8'
  )
  const time = ellapsedTime(ms)
  process.stdout.write(`${time === 0 ? '' : `${time}s `}[DONE]\n`)

  broadcastHMR({ type: 'hmr-reload-app' })
  let nodes = ''

  const saving = global.figma.saving[layout.id]
  if (saving) {
    nodes = JSON.stringify(saving.nodes)
    delete global.figma.saving[layout.id]
  } else {
    // apply all nodes to figma
    // parse and traverse while applying all fnode to figma
    const parsed = parse(file, {
      sourceType: 'module',
      plugins: ['jsx', 'typescript'],
    })

    traverse(parsed, {
      enter: (path) => {
        const c = path.node
        if (
          c.type === 'JSXElement' &&
          c.openingElement.name.type === 'JSXIdentifier' &&
          c.start &&
          c.end
        ) {
          if (c.openingElement.name.name === 'fnode') {
            let node_id = ''
            for (let a of c.openingElement.attributes) {
              if (
                a.type === 'JSXAttribute' &&
                a.name.type === 'JSXIdentifier' &&
                a.value &&
                a.value.type === 'StringLiteral' &&
                a.name.name === 'id'
              ) {
                node_id = a.value.value
              }
            }

            if (node_id) {
              const start = c.children[0].start
              const end = c.children[c.children.length - 1].end

              if (start && end) {
                let childrenGenerated = false
                const childrens = deepClone(c.children)
                traverse(
                  {
                    start: start,
                    end: end,
                    type: 'File',
                    program: {
                      sourceType: 'module',
                      start: start,
                      end: end,
                      type: 'Program',
                      body: childrens,
                    },
                  } as any,
                  {
                    enter: (path) => {
                      const c = path.node
                      if (c.type === 'JSXElement') {
                        if (
                          c.openingElement.name.type === 'JSXIdentifier' &&
                          c.openingElement.name.name === 'fnode'
                        ) {
                          let id = ''
                          for (let e of c.openingElement.attributes) {
                            if (
                              e.type === 'JSXAttribute' &&
                              e.name.name === 'id' &&
                              e.value?.type === 'StringLiteral'
                            ) {
                              id = e.value.value
                            }
                          }
                          if (id !== node_id) {
                            if (childrenGenerated) {
                              path.remove()
                            } else {
                              childrenGenerated = true
                              path.replaceWithSourceString(`{children}`)
                            }
                          }
                        }
                      }
                    },
                  }
                )

                const result: string[] = []
                for (let i of childrens) {
                  result.push(generate(i).code)
                }
                askFigmaBase(
                  async (x) => {
                    await x.askFigma(
                      (p) => {
                        const node = figma.getNodeById(p.id)
                        if (node) {
                          node.setPluginData('f-html', p.code)
                        }
                      },
                      { id: x.id, code: x.code }
                    )
                  },
                  {
                    id: node_id,
                    code: result
                      .join('\n')
                      .replace(/\{\s+children\s+\}/, '{children}'),
                  }
                )
              }
            }
          }
        }
      },
    })
    // and then fetch it again
    nodes = JSON.stringify(
      await askFigmaBase(
        (x) => {
          return x.figma.tools.fetchNodeMap(x.id)
        },
        { id }
      )
    )
  }

  await global.dev.history.pushChange('layout', layout.id, file, nodes)
}
