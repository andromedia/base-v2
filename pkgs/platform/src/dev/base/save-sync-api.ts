import { dirs, log } from 'boot'
import { transform } from 'esbuild'
import { readFile } from 'fs-extra'
import { ellapsedTime } from 'libs'
import trim from 'lodash.trim'
import { join } from 'path'
import { CustomGlobal } from '../../server'

declare const global: CustomGlobal

export const saveSyncAPI = async (
  type: 'api' | 'layout' | 'page',
  id: string
) => {
  const ms = new Date().getTime()
  const filePath = `/app/web/src/base/${type}/${id}_api.ts`
  log('refresh', `🏗️  ${filePath} `, false)

  const source = await readFile(join(dirs.root, filePath), 'utf-8')
  const obj = global.cache[type][id]
  if (obj) {
    const sol = await transform(source, { loader: 'ts' })
    new Function(
      `if (typeof this === 'object') this.serverOnLoad = ${trim(sol.code, ';')}`
    ).bind(obj)()
  }

  const time = ellapsedTime(ms)
  process.stdout.write(`${time === 0 ? '' : `${time}s `}[DONE]\n`)
}
