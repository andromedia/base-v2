import { ParentThread } from '../../../builder/src/thread'
import { CustomGlobal, ServerInstance } from '../server'
import { devHMRRoute, devHMRWsRoute } from './dev-hmr'
import { devRouteComponent } from './page/dev-component'
import { devRoutePage } from './page/dev-page'
import { startSaveSync } from './base/save-sync'
import { BaseHistory } from './base/history'
declare const global: CustomGlobal

export const startDev = async (
  server: ServerInstance,
  parent?: ParentThread
) => {
  global.dev = {
    history: new BaseHistory(),
    sync: startSaveSync(),
  }

  server.get('/__hmr', { websocket: true }, devHMRWsRoute as any)
  server.get('/__hmr/*', devHMRRoute)
  server.get('/__dev/page/:action', devRoutePage)
  server.get('/__dev/page/:action/:id', devRoutePage)
  server.get('/__dev/component/:action', devRouteComponent)
  server.get('/__dev/component/:action/:id', devRouteComponent)
}
