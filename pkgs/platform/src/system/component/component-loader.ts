import { parse } from '@babel/parser'
import { dirs, log } from 'boot'
import { createHash } from 'crypto'
import { pathExists, readFile, rename, writeFile } from 'fs-extra'
import get from 'lodash.get'
import { join } from 'path'
import { cache } from '../init'
import Store from '../session/lib/store'
import { compileSingleComponent } from './component-compiler'

const localStorage = new Store(join(dirs.app.web, 'caches', 'component.db'))

export const loadAllComponent = async (
  list: string[],
  componentPath: string,
  c: typeof cache
) => {
  const oldComps = list.filter((e) => e.endsWith('.html'))
  if (oldComps.length > 0) {
    log(
      'platform',
      `Upgrading ${oldComps.length} component${
        oldComps.length > 1 ? 's' : ''
      } `,
      false
    )

    const compDir = join(dirs.app.web, 'src', 'components')
    const upgrading: Promise<any>[] = []
    for (let i of oldComps) {
      const tsxFile = join(compDir, i.substr(0, i.length - 5) + '.tsx')
      const newFile = join(compDir, i.substr(0, i.length - 5) + '.jsx')
      upgrading.push(
        new Promise<void>(async (resolve) => {
          await rename(join(compDir, i), newFile)
          if (await pathExists(tsxFile)) {
            let tsx = await readFile(tsxFile, 'utf-8')
            tsx = tsx.replace(
              'return eval(_component.render)',
              'return new Function(_component.render).bind(_component)()'
            )
            await writeFile(tsxFile, tsx)
          }

          resolve()
        })
      )
    }
    await Promise.all(upgrading)
    process.stdout.write(' OK\n')
  }

  const comps = list
    .map((i) => {
      if (i.endsWith('.html')) {
        return i.substr(0, i.length - 5) + '.jsx'
      }
      return i
    })
    .filter((e) => e.endsWith('.jsx'))

  log(
    'platform',
    `Loading ${comps.length} component${comps.length > 1 ? 's' : ''} `,
    false
  )

  const externalSource = await readFile(
    join(dirs.app.web, 'src', 'external.tsx'),
    'utf-8'
  )

  const parsed = parse(externalSource, {
    sourceType: 'module',
  })
  const externalImportMap: Record<string, string> = {}
  for (let p of get(parsed, 'program.body.0.declaration.properties')) {
    const key: string = get(p, 'key.value')
    const value: string = get(p, 'value.body.elements.0.arguments.0.value')

    if (key && value) {
      if (value.startsWith('./components/')) {
        externalImportMap[value.substr('./components/'.length)] = key
      }
    }
  }

  const promises: Promise<any>[] = []
  for (let [i, p] of Object.entries(comps)) {
    promises.push(
      new Promise<void>(async (resolve) => {
        if (parseInt(i) % 10 === 0) {
          process.stdout.write('.')
        }

        const fpath = join(componentPath, p)
        const compName = p.substr(0, p.length - 4)
        const source = prepComp(await readFile(fpath, 'utf-8'))
        const hash = md5(source)

        let result = { code: '', map: '' }
        if (hash !== localStorage.get(`comp-${compName}-hash`)) {
          try {
            result = await compileSingleComponent(compName, source)
            localStorage.set(`comp-${compName}-code`, result.code)
            localStorage.set(`comp-${compName}-map`, result.map)
            localStorage.set(`comp-${compName}-hash`, hash)
          } catch (e) {
            console.log('')
            log(
              'boot',
              `Failed to parse: app/web/src/components/${compName}.jsx`
            )
            console.log(e)
            log('boot', `Loading Components `, false)
          }
        } else {
          result = {
            code: localStorage.get(`comp-${compName}-code`),
            map: localStorage.get(`comp-${compName}-map`),
          }
        }

        if (externalImportMap[compName]) {
          c.component[externalImportMap[compName]] = result
        } else {
          c.component[compName] = result
        }

        resolve()
      })
    )
  }

  await Promise.all(promises)

  process.stdout.write(' OK\n')
}

const md5 = (text: string) => {
  return createHash('md5').update(text).digest('hex')
}

const prepComp = (code: string) => {
  return `
const ccx_component = () => {
  const __render__ = (
    db, 
    api,
    action,
    runAction,
    h,
    fragment,
    row,
    layout,
    user,
    params, 
    css,
    meta
  ) => { 
    return <>${code}</>;
  }
  const [_, setRender] = useState({})

  let result = {
    page: jsx('fragment', null, null),
    effects: new Set(),
  }
  try {
    result = window.renderCMS(
      __render__, 
      typeof meta === 'undefined' ? {} : meta,
      { 
        defer: false, 
        type: 'component',
        params
      }
    )

  } catch (e) {
    console.error(__render__, e)
    result = {
      page: jsx('pre', {className:"p-4 text-red-500"},e + ''),
      effects: new Set(),
    }
  }

  if (result) { 
    if (result.loading) {
      result.loading().then(() => setRender({}))
    }
    return result.page;
  }
  return () => jsx('pre', {className:"p-4 text-red-500"},'Render Failed')
}`
}
