import { transformAsync, TransformOptions } from '@babel/core'

export const compileSingleComponent = async (
  name: string,
  jsx: string,
  modernBrowser = false
) => {
  const transformOpt: TransformOptions = {
    minified: true,
    sourceMaps: true,
    presets: [
      [
        '@babel/preset-react',
        {
          pragma: 'h',
          pragmaFrag: 'fragment',
        },
      ],
      [
        '@babel/env',
        {
          targets: {
            browsers: [modernBrowser ? 'defaults' : 'Chrome <= 45'],
          },
          useBuiltIns: 'entry',
          corejs: { version: '3.8', proposals: true },
        },
      ],
      [
        '@babel/preset-typescript',
        {
          isTSX: true,
          allExtensions: true,
          jsxPragma: 'h',
          jsxPragmaFrag: 'fragment',
          allowNamespaces: true,
        },
      ],
    ],
  }

  const result = await transformAsync(
    `${jsx.replace(/<!--([\s\S])*?-->/g, '')}`,
    transformOpt
  )

  let code = result?.code || ''
  if (code && code.startsWith('"use strict";')) {
    code = code.substr('"use strict";'.length)
  }

  if (result && result.map) result.map.sources[0] = `/__component/${name}.js`
  return {
    code,
    map: JSON.stringify(result?.map),
  }
}
