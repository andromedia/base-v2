import { waitUntil } from 'libs'
import { CustomGlobal } from '../server'

declare const global: CustomGlobal
export const routeComponent = async (req, reply) => {
  const params = req.params as { id: string }
  if (params.id.endsWith('.js')) {
    const id = params.id.substr(0, params.id.length - 3)

    if (global.cache.component[id]) {
      reply.send(global.cache.component[id].code)
    } else {
      reply.send('');
    }
  }
  return
}
