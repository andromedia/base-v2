import { dirs, log } from 'boot'
import { pathExists, readdir, readFile, remove, writeFile } from 'fs-extra'
import { join } from 'path'
import { CustomGlobal } from '../../server'
import zlib from 'zlib'

declare const global: CustomGlobal
export const loadAllFigmaImages = async () => {
  const imgdir = join(dirs.app.web, 'figma', 'imgs')
  const imgs = await readdir(imgdir)
  const rawImages = {}
  const imageMaps = global.cache.figma.imageMaps
  const bgMaps = global.cache.figma.bgMaps
  log(
    'platform',
    `Loading ${imgs.length} Figma Image${imgs.length > 1 ? 's' : ''} `,
    false
  )
  let i = 0
  const promises: any[] = []
  for (let imgpath of imgs) {
    if (imgpath.indexOf('____') >= 0) {
      const imgarr = imgpath.split('____')
      const main = imgarr[1]
      const clone = imgarr[0]

      imageMaps[clone] = { to: main }
      continue
    }

    promises.push(
      new Promise<void>(async (resolve) => {
        if (await pathExists(join(imgdir, imgpath))) {
          const file = await readFile(join(imgdir, imgpath))
          const text = file.toString('utf-8')

          if (imgpath.startsWith('bg-')) {
            const bgname = imgpath.substr(3)
            if (!bgMaps[bgname]) {
              bgMaps[bgname] = { raw: file }
              Promise.all([
                new Promise<void>((resolve) => {
                  zlib.brotliCompress(file, {}, (_, result) => {
                    bgMaps[bgname].br = result
                    resolve()
                  })
                }),
                new Promise<void>((resolve) => {
                  zlib.gzip(file, {}, (_, result) => {
                    bgMaps[bgname].gz = result
                    resolve()
                  })
                }),
              ])
            }
            resolve()
            if (i++ % 40 === 0 || i === imgs.length - 1) {
              process.stdout.write('.')
            }
            return
          }

          if (!rawImages[text]) {
            rawImages[text] = imgpath
            imageMaps[imgpath] = { raw: file }
            Promise.all([
              new Promise<void>((resolve) => {
                zlib.brotliCompress(file, {}, (_, result) => {
                  imageMaps[imgpath].br = result
                  resolve()
                })
              }),
              new Promise<void>((resolve) => {
                zlib.gzip(file, {}, (_, result) => {
                  imageMaps[imgpath].gz = result
                  resolve()
                })
              }),
            ])
          } else {
            const main = rawImages[text]
            if (main) {
              await remove(join(imgdir, imgpath))
              await writeFile(join(imgdir, `${imgpath}____${main}`), '')
              imageMaps[imgpath] = { to: main }
            }
          }
        }
        resolve()

        if (i++ % 40 === 0 || i === imgs.length - 1) {
          process.stdout.write('.')
        }
      })
    )
  }

  await Promise.all(promises)

  process.stdout.write(' OK\n')
}
