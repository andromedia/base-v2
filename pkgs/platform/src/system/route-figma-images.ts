import { dirs } from 'boot'
import { FastifyReply, FastifyRequest } from 'fastify'
import fs from 'fs'
import { ensureDir, pathExists, readdir, readFile, remove } from 'fs-extra'
import mime from 'mime-types'
import { join } from 'path'
import { askFigmaBase } from '../dev/dev-hmr'
import zlib from 'zlib'
import { prepareFigma } from '../dev/figma/dev-figma-route'
import { CustomGlobal } from '../server'

declare const global: CustomGlobal

export const routeFigmaImages = async (
  req: FastifyRequest,
  reply: FastifyReply
) => {
  prepareFigma()
  const urls = req.url.split('/')

  const imageMaps = global.cache.figma.imageMaps

  let node = ''
  let type = ''
  let img = { gz: null as any, br: null as any } as any
  if (urls[urls.length - 1].indexOf('bg') === 0) {
    img = await parseFigmaBg({ reply, urls })
  } else {
    const fimg = await parseFigmaUrl({ reply, urls })
    node = fimg.node
    type = fimg.type

    img = imageMaps[node]
    if (img && img.to) {
      img = imageMaps[img.to]
    }
  }

  if (img) {
    const gz = img.gz
    const br = img.br

    const mimeType = mime.types[type]
    if (mimeType) {
      reply.type(mimeType)
    }

    const acceptEncoding = req.headers['accept-encoding']
    if (acceptEncoding) {
      if (!!br && acceptEncoding.indexOf('br') >= 0) {
        reply.header('content-encoding', 'br')
        reply.send(br)
        return true
      }

      if (!!gz && acceptEncoding.indexOf('gz') >= 0) {
        reply.header('content-encoding', 'gzip')
        reply.send(gz)
        return true
      }
    }

    reply.send(img.raw)
    return
  }

  reply.code(404)
}

const parseFigmaBg = async (opt: { urls: string[]; reply: FastifyReply }) => {
  const rawname = opt.urls[opt.urls.length - 1]
  const hash = rawname.substr(3)
  const bgpath = join(dirs.app.web, 'figma', 'imgs', `bg-${rawname}`)

  if (global.cache.figma.bgMaps[hash]) {
    return global.cache.figma.bgMaps[hash]
  }

  if (!(await pathExists(bgpath))) {
    await new Promise<void>(async (resolve) => {
      const bin = (await askFigmaBase(
        async (x) => {
          return await x.askFigma(
            async (p) => {
              const image = figma.getImageByHash(p.hash)
              return Array.from(await image.getBytesAsync())
            },
            { hash: x.hash }
          )
        },
        { hash }
      )) as any

      const chunk = Uint8Array.from(bin)
      fs.appendFile(bgpath, Buffer.from(chunk), function (err) {
        resolve()
      })
    })
  }

  const imageMaps = global.cache.figma.bgMaps
  if (await pathExists(bgpath)) {
    const file = await readFile(bgpath)
    imageMaps[bgpath] = { raw: file }
    imageMaps[bgpath].raw
    await Promise.all([
      new Promise<void>((resolve) => {
        zlib.brotliCompress(file, {}, (_, result) => {
          imageMaps[bgpath].br = result
          resolve()
        })
      }),
      new Promise<void>((resolve) => {
        zlib.gzip(file, {}, (_, result) => {
          imageMaps[bgpath].gz = result
          resolve()
        })
      }),
    ])
  }

  if (global.cache.figma.bgMaps[hash]) {
    return global.cache.figma.bgMaps[hash]
  }
}

const parseFigmaUrl = async (opt: { urls: string[]; reply: FastifyReply }) => {
  const { urls, reply } = opt

  const imageMaps = global.cache.figma.imageMaps
  if (urls.length < 3) {
    reply.code(404)
    return reply.send('NOT FOUND')
  }

  let node = urls[2]

  const last = (urls.pop() || '').split('.')
  if (last.length < 3) {
    reply.code(404)
    return reply.send('NOT FOUND')
  }

  const type = last.pop() || ''
  const scale = (last.pop() || '').replace('x', '')
  const narr = (last.pop() || '').split('_')
  let node_id = ''
  if (narr.length === 2) {
    node_id = `${narr[0]}:${narr[1]}`
  } else if (narr.length === 4) {
    node = `${narr[2]}_${narr[3]}.x${scale}.${type}`
    node_id = `${narr[2]}:${narr[3]}`
  }
  if (!imageMaps[node]) {
    const fetchedImg = await fetchFigmaImage({
      node_id,
      type,
      scale,
    })

    const imgpath = await saveFigmaImage({
      node_id,
      type,
      scale,
      value: fetchedImg,
    })

    const file = await readFile(imgpath)
    imageMaps[imgpath] = { raw: file }
    imageMaps[imgpath].raw
    await Promise.all([
      new Promise<void>((resolve) => {
        zlib.brotliCompress(file, {}, (_, result) => {
          imageMaps[imgpath].br = result
          resolve()
        })
      }),
      new Promise<void>((resolve) => {
        zlib.gzip(file, {}, (_, result) => {
          imageMaps[imgpath].gz = result
          resolve()
        })
      }),
    ])
  }
  return { node, node_id, type }
}

export const fetchFigmaImage = async (opt: {
  node_id: string
  type: string
  scale: string
}) => {
  const { node_id, type, scale } = opt
  return await askFigmaBase(
    async (x) => {
      return await x.askFigma(
        async (d) => {
          const node: any = figma.root.findOne((e) => e.id === d.node_id)

          if (node) {
            const settings = {
              format: d.type,
            }
            if (d.type === 'PNG') {
              settings['constraint'] = {
                type: 'SCALE',
                value: parseInt(d.scale),
              }
            }
            const bin = await node.exportAsync(settings)
            return {
              type: d.type,
              scale: d.scale,
              value: Array.from(bin),
            }
          }
        },
        { ...x.data }
      )
    },
    {
      data: {
        type: type.toUpperCase(),
        node_id,
        scale,
      },
    }
  )
}

export const saveFigmaImage = (data: {
  node_id: string
  value: any
  type: string
  scale: string
}): Promise<string> => {
  return new Promise<string>(async (resolve) => {
    let bin = data.value
    const chunk = Uint8Array.from(bin)
    if (chunk.length === 0) {
      return
    }

    let { node_id, type, scale } = data

    node_id = node_id.split(';').pop() || ''
    const node_url = node_id.replace(/\W+/g, '_')
    const node = `${node_url}.x${scale}.${type.toLowerCase()}`
    const dir = join(dirs.app.web, 'figma', 'imgs')
    const file = join(dirs.app.web, 'figma', 'imgs', node)

    await ensureDir(dir)

    const files = await readdir(dir)
    for (let i of files) {
      if (i.indexOf(`${node_url}.`) === 0) {
        await remove(join(dir, i))
      }
    }

    fs.appendFile(file, Buffer.from(chunk), function (err) {
      resolve(file)
    })
  })
}

// const serveFigmaBg = async (
//   req: FastifyRequest,
//   reply: FastifyReply,
//   rawname: string,
//   figma: IFigma
// ) => {
//   const hash = rawname.substr(3)

//   const dir = join(dirs.app.web, 'figma', 'imgs')
//   const file = join(dirs.app.web, 'figma', 'imgs', `bg-${hash}`)

//   if (serveCached(req, reply, file)) {
//     return
//   }

//   if (await pathExists(file)) {
//     reply.sendFile(`bg-${hash}`, dir)
//     return
//   }

//   // for (let ws of figma.ws['figma']) {
//   //   ws.send(
//   //     JSON.stringify({
//   //       type: 'get-bg-image',
//   //       data: {
//   //         hash,
//   //       },
//   //     })
//   //   )
//   // }

//   await waitUntil(() => {
//     return figma.bgImages.filter((e) => e.hash === hash).length > 0
//   })

//   let bin = []
//   for (let i in figma.bgImages) {
//     const e = figma.bgImages[i]
//     if (e.hash === hash) {
//       figma.bgImages.splice(i as any, 1)
//       bin = e.value
//       break
//     }
//   }

//   const chunk = Uint8Array.from(bin)
//   if (chunk.length === 0) {
//     reply.send('NOT FOUND')
//     return
//   }

//   await ensureDir(dir)

//   const files = await readdir(dir)
//   for (let i of files) {
//     if (i === `bg-${hash}`) {
//       await remove(join(dir, i))
//     }
//   }

//   fs.appendFile(file, Buffer.from(chunk), function (err) {
//     reply.sendFile(`bg-${hash}`, dir)
//   })
// }
