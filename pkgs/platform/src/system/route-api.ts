import { log } from 'boot'
import { FastifyReply, FastifyRequest } from 'fastify'
import { matchRoute } from 'libs'
import { ext as extServer } from 'server'
import { api } from 'web.utils/src/api'
import { CustomGlobal } from '../server'
import { serialize } from '../utils'
import { convertOldQueryParams } from './route-data'
import { loadSession } from './session/loader'

const { db } = require('db')

if (!db.query) {
  db.query = async function (sql: string) {
    let final = { result: new Promise<null>((resolve) => resolve(null)), db }
    new Function(`this.result = this.db.$queryRaw\`${sql}\``).bind(final)()
    return await final.result
  }
  for (let [k, v] of Object.entries(db) as any) {
    if (typeof v === 'object' && v && v.findMany) {
      v.query = (params: any) => {
        const newparams = convertOldQueryParams(k, params)
        return v.findMany(newparams)
      }
    }
  }
}

const ext =
  extServer && (extServer as any).default
    ? (extServer as any).default
    : extServer

declare const global: CustomGlobal

export const routeAPI = async (req: FastifyRequest, reply: FastifyReply) => {
  let url = req.url.split('?')[0]
  for (let page of Object.values(global.cache.api)) {
    if (page.url) {
      const params = matchRoute(url, page.url)
      if (params && page.serverOnLoad) {
        const session = await loadSession(req, reply)

        const solLog = (...args: any[]) => {
          let output: string[] = []
          for (let i of args) {
            if (typeof i === 'object') {
              output.push(serialize(i))
            } else {
              output.push(i)
            }
          }
          reply.send(output.join('\n'))
        }
        try {
          await page.serverOnLoad({
            template: null,
            params: params,
            render: async (_template, _params) => {},
            db,
            req,
            reply,
            user: session,
            ext,
            api,
            isDev: global.mode === 'dev',
            log: solLog,
          })
        } catch (e) {
          log(
            'error',
            `ERROR: app/web/src/base/api/${page.id}_api.ts - ${page.url}\n `
          )
          console.log(e)
        }
        return true
      }
    }
  }
  return false
}
