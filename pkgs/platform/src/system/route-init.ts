import { FastifyReply, FastifyRequest } from 'fastify'
import { matchRoute, waitUntil } from 'libs'
import cloneDeep from 'lodash.clonedeep'
import { Packr } from 'msgpackr'
import { ext as extServer } from 'server'
import { api } from 'web.utils/src/api'
import { CustomGlobal } from '../server'
import { loadSession } from '../system/session/loader'
import { serialize } from '../utils'
import { renderParamsLayout } from './route-layout'

const { db } = require('db')
const ext =
  extServer && (extServer as any).default
    ? (extServer as any).default
    : extServer

declare const global: CustomGlobal
export const routeInit = async (req: FastifyRequest, reply: FastifyReply) => {
  let url = req.url.split('?')[0].substr('/__init'.length)

  const session = await loadSession(req, reply)

  await waitUntil(() => global.initialData)
  const initialData = cloneDeep(global.initialData)
  if (session.user) {
    initialData.user = JSON.stringify(session.user)
  }

  const solLog = (...args: any[]) => {
    let output: string[] = []
    for (let i of args) {
      if (typeof i === 'object') {
        output.push(serialize(i))
      } else {
        output.push(i)
      }
    }
    reply.send(output.join('\n'))
  }

  for (let [key, page] of Object.entries(global.cache.page)) {
    if (page && page.url) {
      let matchedParams = matchRoute(url, page.url)
      if (matchedParams) {
        initialData.params = matchedParams
        initialData.cms_id = page.id

        const layout = initialData.cms_layouts[page.layout_id]
        if (layout) {
          const cachedLayout = global.cache.layout[layout.id]
          if (!layout.source && cachedLayout) {
            const sourceMapUrl = `/__layout/${layout.id}.js.map`
            layout.source =
              cachedLayout.jsx.code + `\n\n//# sourceMappingURL=${sourceMapUrl}`
          }

          const sol = cachedLayout.serverOnLoad
          if (sol) {
            initialData.params = await renderParamsLayout(
              matchedParams,
              cachedLayout,
              req,
              reply
            )
          }
        }

        if (page.serverOnLoad) {
          page.serverOnLoad({
            template: null,
            params: initialData.params,
            render: async (_template, _params) => {
              if (_params) {
                initialData.params = _params
              }
              reply.type('application/javascript')
              const packer = new Packr({})
              reply.send(
                `window.cms_base_pack = ${JSON.stringify(
                  packer.pack(initialData).toJSON().data
                )}`
              )
            },
            db,
            req,
            reply,
            user: session,
            ext,
            api,
            isDev: global.mode === 'dev',
            log: solLog,
          })
          return
        }
      }
    } else {
    }
  }

  reply.type('application/javascript')
  const packer = new Packr({})
  const content = Buffer.from(
    `window.cms_base_pack = ${JSON.stringify(
      packer.pack(initialData).toJSON().data
    )}`,
    'utf-8'
  )

  // if (req.headers['accept-encoding'].indexOf('br') >= 0) {
  //   reply.header('content-encoding', 'br')

  //   zlib.brotliCompress(content, {}, (_, result) => {
  //     reply.send(result)
  //   })
  //   return
  // }

  // if (req.headers['accept-encoding'].indexOf('gz') >= 0) {
  //   reply.header('content-encoding', 'gz')
  //   zlib.gzip(content, {}, (_, result) => {
  //     reply.send(result)
  //   })
  //   return
  // }

  reply.send(content)
}
