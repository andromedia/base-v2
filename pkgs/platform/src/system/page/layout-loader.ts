import { dirs, log } from 'boot'
import { createHash } from 'crypto'
import { transform } from 'esbuild'
import { pathExists, readFile } from 'fs-extra'
import trim from 'lodash.trim'
import { join } from 'path'
import { cache, Layout, Page } from '../init'
import Store from '../session/lib/store'
import { compileSinglePage, preProcessJSX } from './page-compiler'

export const pageStorage = new Store(join(dirs.app.web, 'caches', 'page.db'))

export const loadAllLayouts = async (
  list: string[],
  pagePath: string,
  c: typeof cache
) => {
  const layouts = list.filter((e) => e.endsWith('.json'))
  log(
    'platform',
    `Loading ${layouts.length} layout${layouts.length > 1 ? 's' : ''} `,
    false
  )

  const promises: Promise<any>[] = []
  for (let [i, p] of Object.entries(layouts)) {
    promises.push(
      new Promise<void>(async (resolve) => {
        if (parseInt(i) % 15 === 0) {
          process.stdout.write('.')
        }
        await reloadSingleLayout(p, pagePath, c)
        resolve()
      })
    )
  }
  await Promise.all(promises)

  process.stdout.write(' OK\n')
}

export const reloadSingleLayout = async (
  page_id: string,
  pagePath: string,
  c: typeof cache
) => {
  let jsonFile = page_id.endsWith('.json') ? page_id : page_id + '.json'
  const source = await readFile(join(pagePath, jsonFile), 'utf-8')
  const layout: Layout = JSON.parse(source)

  try {
    if (layout) {
      const fpath = join(pagePath, `${layout.id}.jsx`)
      if (await pathExists(fpath)) {
        const jsx = prepLayoutJsx(layout, await readFile(fpath, 'utf-8'))

        if (jsx) {
          const hash = md5(jsx)

          if (hash !== pageStorage.get(`page-${layout.id}-hash`)) {
            const compiled = await compileSinglePage(layout.id, jsx)

            if (compiled) {
              layout.jsx = compiled
              pageStorage.set(`page-${layout.id}-code`, layout.jsx.code)
              pageStorage.set(`page-${layout.id}-map`, layout.jsx.map)
              pageStorage.set(`page-${layout.id}-hash`, hash)
            }
          } else {
            layout.jsx = {
              raw: jsx,
              code: pageStorage.get(`page-${layout.id}-code`) || '',
              map: pageStorage.get(`page-${layout.id}-map`) || '',
            }
          }
        }
      }
      if (await pathExists(join(pagePath, `${layout.id}_api.ts`))) {
        const sol = await transform(
          await readFile(join(pagePath, `${layout.id}_api.ts`), 'utf-8'),
          { loader: 'ts' }
        )

        new Function(
          `if (typeof this === 'object') this.serverOnLoad = ${trim(
            sol.code,
            ';'
          )}`
        ).bind(layout)()
      }
      c.layout[layout.id] = layout
    } else {
      console.log(layout)
    }
  } catch (e: any) {
    console.log(e)
  }
}

const md5 = (text: string) => {
  return createHash('md5').update(text).digest('hex')
}

const prepLayoutJsx = (layout: Layout, jsx: string) => {
  try {
    const { meta, code } = preProcessJSX(layout, jsx)
    return `
// layout: ${layout.id} | name: ${layout.name}
if (window.cms_layouts['${layout.id}']) {
  window.cms_layouts['${
    layout.id
  }'].running = { cache: null, init: false, mobx: {} };
  window.cms_layouts['${layout.id}'].render = function (
    db, 
    api,
    action,
    runAction,
    h,
    fragment,
    row,
    layout,
    user,
    params, 
    css,
    meta, 
    children
  ) {
    return ${trim(code.trim(), ';').trim()}
  }
  window.cms_layouts['${layout.id}'].render.child_meta = ${meta};
}`
  } catch (e: any) {
    process.stdout.write('\n')
    log(
      'error',
      `ERROR: app/web/src/base/layouts/${layout.id}.jsx | ${layout.name}`
    )
    log('error', e.message)
  }
}
