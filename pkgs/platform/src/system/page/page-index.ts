import { CustomGlobal } from '../../server'

declare const global: CustomGlobal

export const serveIndexHtml = async () => {
  global.cache.index
}
