import { dirs, log } from 'boot'
import { createHash } from 'crypto'
import { transform } from 'esbuild'
import { pathExists, readFile, rename, writeFile } from 'fs-extra'
import { snakeCase } from 'lodash'
import trim from 'lodash.trim'
import { join } from 'path'
import { cache, Page } from '../init'
import Store from '../session/lib/store'
import { compileSinglePage, preProcessJSX } from './page-compiler'

export const pageStorage = new Store(join(dirs.app.web, 'caches', 'page.db'))

export const pageJSXNameMap = {}

export const loadAllPages = async (
  list: string[],
  pagePath: string,
  c: typeof cache
) => {
  const pages = list.filter((e) => e.endsWith('.json'))
  log(
    'platform',
    `Loading ${pages.length} page${pages.length > 1 ? 's' : ''} `,
    false
  )

  list
    .filter((e) => e.endsWith('.jsx'))
    .forEach((e) => {
      const id = e.substr(0, 5)
      const name = e.substr(5, e.length - 9)
      pageJSXNameMap[id] = name
    })

  const promises: Promise<any>[] = []
  for (let [i, p] of Object.entries(pages)) {
    promises.push(
      new Promise<void>(async (resolve) => {
        if (parseInt(i) % 15 === 0) {
          process.stdout.write('.')
        }
        await reloadSinglePage(p, pagePath, c)
        resolve()
      })
    )
  }
  await Promise.all(promises)

  process.stdout.write(' OK\n')
}

export const reloadSinglePage = async (
  pageID: string,
  pagePath: string,
  c: typeof cache
) => {
  let jsonFile = pageID.endsWith('.json') ? pageID : pageID + '.json'
  const source = await readFile(join(pagePath, jsonFile), 'utf-8')

  try {
    const page: Page = JSON.parse(source)

    if (page) {
      const name = pageJSXNameMap[page.id]
      if (typeof name !== undefined) {
        const pageName = '-' + snakeCase(page.name).replace(/_/gi, '-')
        if (name !== page.name) {
          if (await pathExists(join(pagePath, `${page.id}${name || ''}.jsx`))) {
            await rename(
              join(pagePath, `${page.id}${name || ''}.jsx`),
              join(pagePath, `${page.id}${pageName}.jsx`)
            )
          } else {
            await writeFile(
              join(pagePath, `${page.id}${pageName}.jsx`),
              `;<>
  <effect meta={{}} run={async () => {}} />
  <div></div>
</>`
            )
          }

          pageJSXNameMap[page.id] = pageName
        }

        const fpath = join(pagePath, `${page.id}${pageName}.jsx`)
        if (await pathExists(fpath)) {
          const jsx = prepPageJsx(page, await readFile(fpath, 'utf-8'))

          if (jsx) {
            const hash = md5(jsx)
            if (hash !== pageStorage.get(`page-${page.id}-hash`)) {
              const compiled = await compileSinglePage(page.id, jsx)

              if (compiled) {
                page.jsx = compiled
                pageStorage.set(`page-${page.id}-code`, page.jsx.code)
                pageStorage.set(`page-${page.id}-map`, page.jsx.map)
                pageStorage.set(`page-${page.id}-hash`, hash)
              }
            } else {
              page.jsx = {
                raw: jsx,
                code: pageStorage.get(`page-${page.id}-code`) || '',
                map: pageStorage.get(`page-${page.id}-map`) || '',
              }
            }
          }
        }
        if (await pathExists(join(pagePath, `${page.id}_api.ts`))) {
          const sol = await transform(
            await readFile(join(pagePath, `${page.id}_api.ts`), 'utf-8'),
            { loader: 'ts' }
          )

          new Function(
            `if (typeof this === 'object') this.serverOnLoad = ${trim(
              sol.code,
              ';'
            )}`
          ).bind(page)()
        }
        c.page[page.id] = page
      }
    } else {
      console.log(page)
    }
  } catch (e: any) {
    console.log('')
    log('error', `Failed to load page: app/web/src/base/page/${jsonFile} `)
    console.log(e)
  }
}

const md5 = (text: string) => {
  return createHash('md5').update(text).digest('hex')
}

const prepPageJsx = (page: Page, jsx: string) => {
  try {
    const { meta, code } = preProcessJSX(page, jsx)
    return `
// page: ${page.id} | layout: ${page.layout_id} | url: ${page.url}
if (window.cms_pages['${page.url}']) {
  window.cms_pages['${page.url}'].render = function (
    db, 
    api,
    action,
    runAction,
    h,
    fragment,
    row,
    layout,
    user,
    params, 
    css,
    meta
  ) {
    return ${trim(code.trim(), ';').trim()}
  }
  window.cms_pages['${page.url}'].render.child_meta = ${meta};
}`
  } catch (e: any) {
    process.stdout.write('\n')
    log('error', `ERROR: app/web/src/base/page/${page.id}.jsx | ${page.name}`)
    log('error', e.message)
  }
}
