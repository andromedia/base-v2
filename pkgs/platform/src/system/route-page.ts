import { matchRoute, waitUntil } from 'libs'
import { ext as extServer } from 'server'
import { CustomGlobal } from '../server'
import { loadSession } from './session/loader'
import { api } from 'web.utils/src/api'
import { serialize } from 'v8'

const ext =
  extServer && (extServer as any).default
    ? (extServer as any).default
    : extServer

const { db } = require('db')
declare const global: CustomGlobal
export const routePage = async (req, reply) => {
  const params = req.params as { id: string; ext: 'js' | 'js.map' }
  const page = global.cache.page[params.id]
  if (page) {
    if (!page.jsx) {
      await waitUntil(() => !!page.jsx)
    }
    if (params.ext === 'js') {
      const sourceMapUrl = req.url + '.map'
      reply.header('SourceMap', sourceMapUrl)
      reply.type('application/javascript')
      reply.send(page.jsx.code + `\n\n//# sourceMappingURL=${sourceMapUrl}`)
      return
    } else if (params.ext === 'js.map') {
      reply.type('application/json')
      reply.send(page.jsx.map)
      return
    }
  }

  reply.code(404)
  reply.send('404 Not Found')
}

export const routePageParams = async (req, reply) => {
  const params = req.params as { id: string }
  const page = global.cache.page[params.id]
  if (page && page.serverOnLoad && req.body && req.body.url) {
    const url = req.body.url
    const params = matchRoute(url, page.url)

    const session = await loadSession(req, reply)
    if (params && page.serverOnLoad) {
      page.serverOnLoad({
        template: null,
        params: params,
        render: async (_template, _params) => {
          reply.send(_params)
        },
        db,
        req,
        reply,
        user: session,
        ext,
        api,
        isDev: global.mode === 'dev',
        log: console.log,
      })
      return
    }
  }

  reply.code(404)
  reply.send('{}')
}
