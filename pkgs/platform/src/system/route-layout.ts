import { waitUntil } from 'libs'
import { ext as extServer } from 'server'
import { api } from 'web.utils/src/api'
import { CustomGlobal } from '../server'
import { Layout } from './init'
import { loadSession } from './session/loader'

const ext =
  extServer && (extServer as any).default
    ? (extServer as any).default
    : extServer

const { db } = require('db')
declare const global: CustomGlobal
export const routeLayout = async (req, reply) => {
  const params = req.params as { id: string; ext: 'js' | 'js.map' }
  const layout = global.cache.layout[params.id]
  if (layout) {
    if (!layout.jsx) {
      await waitUntil(() => !!layout.jsx)
    }
    if (params.ext === 'js') {
      const sourceMapUrl = req.url + '.map'
      reply.header('SourceMap', sourceMapUrl)
      reply.type('application/javascript')
      reply.send(layout.jsx.code + `\n\n//# sourceMappingURL=${sourceMapUrl}`)
      return
    } else if (params.ext === 'js.map') {
      reply.type('application/json')
      reply.send(layout.jsx.map)
      return
    }
  }

  reply.code(404)
  reply.send('404 Not Found')
}

export const renderParamsLayout = async (
  params,
  layout: Layout,
  req,
  reply
) => {
  if (layout && layout.serverOnLoad) {
    const session = await loadSession(req, reply)
    return await new Promise((resolve) => {
      if (layout && layout.serverOnLoad)
        layout.serverOnLoad({
          template: null,
          params,
          render: async (_template, _params) => {
            resolve(_params)
          },
          db,
          req,
          reply,
          user: session,
          ext,
          api,
          isDev: global.mode === 'dev',
          log: console.log,
        })
    })
  }
}
