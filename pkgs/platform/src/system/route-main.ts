import { FastifyReply, FastifyRequest } from 'fastify'
import { CustomGlobal } from '../server'

declare const global: CustomGlobal

export const routeMain = async (req: FastifyRequest, reply: FastifyReply) => {
  let url = req.url.split('?')[0]
  if (req.headers.accept && req.headers.accept.indexOf('html') >= 0) {
    reply.type('text/html')
    reply.send(prepareBody(url))
    return
  }

  reply.code(404)
  if (req.method === 'get') {
    reply.send(prepareBody(url))
  } else {
    reply.type('application/json')
    reply.send('{"status": "404 - NOT FOUND"}')
  }
}

const prepareBody = (url: string) => {
  return global.cache.index.replace(
    '</title>',
    `</title>
    <script>window.global={};const d=document.createElement("script");d.src="/__init${
      url.startsWith('/') ? url : `/${url}`
    }",document.getElementsByTagName("head")[0].appendChild(d);</script>`
  )
}
