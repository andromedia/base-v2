import { dirs } from 'boot'
import { FastifyReply, FastifyRequest } from 'fastify'
import { lstat } from 'fs/promises'
import mime from 'mime-types'
import { basename, dirname, extname, join } from 'path'
import { CustomGlobal } from '../server'

declare const global: CustomGlobal

export const serveStaticFile = async ({
  url,
  req,
  reply,
}: {
  url: string
  req: FastifyRequest
  reply: FastifyReply
}) => {
  let _url = decodeURIComponent(url)

  if (_url === '/' || _url === '') {
    return false
  }

  if (_url.indexOf('/min-maps') === 0) {
    let publicFile = join(dirs.pkgs.web, 'ext', 'monaco', _url)
    if (await isFile(publicFile)) {
      reply.sendFile(_url, join(dirs.pkgs.web, 'ext', 'monaco'))
    } else {
      reply.code(404)
      reply.send(404)
    }
    return true
  }

  let publicFile = join(dirs.app.web, 'build', 'web', _url)
  if (serveCached(req, reply, publicFile)) {
    return true
  }
  if (await isFile(publicFile)) {
    reply.sendFile(basename(publicFile), dirname(publicFile))
    return true
  }
  return false
}

export const serveCached = (
  req: FastifyRequest,
  reply: FastifyReply,
  publicFile: string
) => {
  const pfile = global.cache.public.raw[publicFile]
  if (pfile) {
    const gz = global.cache.public.gz[publicFile]
    const br = global.cache.public.br[publicFile]

    const type = mime.types[extname(publicFile).substr(1)]
    if (type) {
      reply.type(type)
    }

    const acceptEncoding = req.headers['accept-encoding']
    if (acceptEncoding) {
      if (!!br && acceptEncoding.indexOf('br') >= 0) {
        reply.header('content-encoding', 'br')
        reply.send(br)
        return true
      }

      if (!!gz && acceptEncoding.indexOf('gz') >= 0) {
        reply.header('content-encoding', 'gzip')
        reply.send(gz)
        return true
      }
    }

    reply.send(pfile)
    return true
  }
  return false
}

const isFileCache = new Map<string, boolean>()
export const isFile = async (file: string) => {
  try {
    const status = isFileCache.get(file)
    if (status === undefined) {
      const stat = await lstat(file)
      if (stat.isFile()) {
        isFileCache.set(file, true)
        return true
      } else {
        isFileCache.set(file, false)
        return false
      }
    }
    return status
  } catch (e) {
    return false
  }
}
