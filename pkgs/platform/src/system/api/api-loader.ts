import { log } from 'boot'
import { transform } from 'esbuild'
import { pathExists, readFile } from 'fs-extra'
import { join } from 'path'
import { API, cache } from '../init'
import trim from 'lodash.trim'
export const loadAllAPI = async (
  list: string[],
  apiPath: string,
  c: typeof cache
) => {
  const pages = list.filter((e) => e.endsWith('.json'))
  log(
    'platform',
    `Loading ${pages.length} API${pages.length > 1 ? 's' : ''} `,
    false
  )

  const promises: Promise<any>[] = []
  for (let [i, p] of Object.entries(pages)) {
    promises.push(
      new Promise<void>(async (resolve) => {
        if (parseInt(i) % 10 === 0) {
          process.stdout.write('.')
        }

        const page: API = JSON.parse(await readFile(join(apiPath, p), 'utf-8'))

        if (page) {
          if (await pathExists(join(apiPath, `${page.id}_api.ts`))) {
            const sol = await transform(
              await readFile(join(apiPath, `${page.id}_api.ts`), 'utf-8'),
              { loader: 'ts' }
            )

            const solfunc = new Function(
              `this.serverOnLoad = ${trim(sol.code, ';')}`
            )
            solfunc.bind(page)()
          }

          c.api[page.id] = page
        }
        resolve()
      })
    )
  }

  await Promise.all(promises)

  process.stdout.write(' OK\n')
}
