import { getDMMF } from '@prisma/sdk'
import { dirs, log } from 'boot'
import { ensureDir, pathExists, readdir, readFile } from 'fs-extra'
import klaw from 'klaw'
import { waitUntil } from 'libs'
import get from 'lodash.get'
import { join } from 'path'
import zlib from 'zlib'
import { startMigration } from '../legacy/page-migrator'
import { generateBuildId } from '../routes'
import { CustomGlobal } from '../server'
import { loadAllAPI } from './api/api-loader'
import { loadAllComponent } from './component/component-loader'
import { loadAllFigmaImages } from './figma/image-loader'
import { loadAllLayouts } from './page/layout-loader'
import { loadAllPages } from './page/page-loader'

export type Page = {
  id: string
  name: string
  jsx: { raw: string; code: string; map: string }
  url: string
  layout_id: string
  serverOnLoad?: (args: any) => void
}

declare const global: CustomGlobal

export type API = {
  id: string
  type: 'WS' | 'API'
  url: string
  serverOnLoad: (args: any) => void
}

export type Layout = {
  id: string
  name: string
  jsx: { raw: string; code: string; map: string }
  serverOnLoad?: (args: any) => void
}

export const cache: {
  layout: Record<string, Layout>
  page: Record<string, Page>
  api: Record<string, API>
  component: Record<string, { code: string; map: string }>
  index: string
  figma: {
    bgMaps: Record<string, { raw: any; gz?: any; br?: any }>
    imageMaps: Record<string, { to?: string; raw?: any; gz?: any; br?: any }>
  }
  public: {
    gz: any
    br: any
    raw: any
  }
} = {
  layout: {},
  page: {},
  api: {},
  component: {},
  figma: { imageMaps: {}, bgMaps: {} },
  index: '',
  public: { gz: {}, br: {}, raw: {} },
}

export const oldCmsDir = {
  cms: join(dirs.app.web, 'cms'),
  structures: join(dirs.app.web, 'cms', 'structures'),
  templates: join(dirs.app.web, 'cms', 'templates'),
}
export const baseDir = {
  layout: join(dirs.app.web, 'src', 'base', 'layout'),
  page: join(dirs.app.web, 'src', 'base', 'page'),
  api: join(dirs.app.web, 'src', 'base', 'api'),
  component: join(dirs.app.web, 'src', 'components'),
}

export const reloadCache = async () => {
  if (global.cache !== cache) global.cache = cache
  global.dmmf = await getDMMF({
    datamodelPath: join(dirs.app.db, 'prisma', 'schema.prisma'),
  })

  await generateBuildId()

  cache.layout = {}
  cache.page = {}
  cache.api = {}
  cache.component = {}
  cache.figma = { imageMaps: {}, bgMaps: {} }

  await waitUntil(
    async () =>
      await pathExists(join(dirs.app.web, 'build', 'web', 'index.html'))
  )
  cache.index = await readFile(
    join(dirs.app.web, 'build', 'web', 'index.html'),
    'utf-8'
  )
  cache.index = cache.index.replace(
    `</body>`,
    `<script type="module" src="${global.mainjs}"></script>`
  )

  global.baseUpgrade = {
    mandatory:
      (await pathExists(join(dirs.app.web, 'cms'))) &&
      !(await pathExists(join(dirs.app.web, 'src', 'base', 'page'))),
    migrating: false,
  }

  await Promise.all(
    Object.values(baseDir).map(async (e) => {
      return ensureDir(e)
    })
  )

  if ((await (await readdir(baseDir.page)).length) === 0) {
    global.baseUpgrade.mandatory = true
  }

  compressPublic(cache)

  if (global.mode === 'dev') {
    await applyDevBuild()
  }

  await ensureDir(join(dirs.app.web, 'figma', 'imgs'))

  // make sure old cms is completely gone (migrated)
  // when loading new base
  if (!global.baseUpgrade.mandatory) {
    await initBase()
  } else {
    setupInitDataFromCache()
    waitUntil(() => global.figma.connected).then(startMigration)
    setTimeout(async () => {
      log('base', '')
      log('base', '=== WARNING ===')
      log('base', `Base need to be migrated to new version.`)
      log('base', `Please run Figma Plugin to start automatic migration.`)
      log('base', '')

      log('figma', `Waiting Figma Plugin`)

      await waitUntil(5000)
      if (!global.figma.connected) {
        log('figma', 'Still waiting for Figma Plugin...')
      }
      await waitUntil(5000)
      if (!global.figma.connected) {
        log('figma', 'Please Start Figma Plugin...')
      }
    })
  }
}

export const applyDevBuild = async () => {
  for (let [fpath, file] of Object.entries(global.devBuild?.files || {})) {
    const path = join(dirs.app.web, 'build', 'web', fpath)
    global.cache.public.raw[path] = file.raw
    global.cache.public.br[path] = file.br
    global.cache.public.gz[path] = file.gz
  }
}

export const initBase = async () => {
  for (let [k, v] of Object.entries(baseDir)) {
    const list = await readdir(v)
    switch (k) {
      case 'layout':
        {
          await loadAllLayouts(list, v, cache)
        }
        break
      case 'page':
        {
          await loadAllPages(list, v, cache)
        }
        break
      case 'api':
        {
          await loadAllAPI(list, v, cache)
        }
        break
      case 'component':
        {
          await loadAllComponent(list, v, cache)
        }
        break
    }
    if (global.shouldExit) return
  }
  await loadAllFigmaImages()
  setupInitDataFromCache()
}

export const setupInitDataFromCache = () => {
  const pages = {}
  const layouts = {}

  Object.entries(get(global, 'cache.page', {})).map((e) => {
    const page = e[1] as Page
    pages[page.url] = {
      id: page.id,
      lid: page.layout_id,
      url: page.url,
      sol: !!page.serverOnLoad,
    }
  })

  Object.entries(get(global, 'cache.layout', {})).map((e) => {
    const layout = e[1] as Layout
    layouts[layout.id] = {
      id: layout.id,
      name: layout.name,
    }
  })

  global.initialData = {
    build_id: global.build_id,
    user: JSON.stringify({ role: 'guest' }),
    params: {},
    cms_id: '',
    cms_pages: pages,
    cms_layouts: layouts,
    is_dev: global.mode === 'dev',
    secret: global.secret,
  }
}

export const compressPublic = async (cache: typeof global.cache) => {
  const createCache = async function (this: any, item) {
    if (item.stats.isFile()) {
      const file = await readFile(item.path)
      let newpath = item.path

      if (this && this.newPath && this.oldPath) {
        newpath = join(this.newPath, newpath.substr(this.oldPath.length))
      }
      if (
        item.path.endsWith('.js') ||
        item.path.endsWith('.css') ||
        item.path.endsWith('.svg')
      ) {
        zlib.brotliCompress(file, {}, (_, result) => {
          cache.public.br[newpath] = result
        })
        zlib.gzip(file, {}, (_, result) => {
          cache.public.gz[newpath] = result
        })
      }
      cache.public.raw[newpath] = file
    }
  }

  const promises: any = []

  promises.push(
    new Promise<void>((resolve) => {
      klaw(join(dirs.pkgs.web, 'ext'))
        .on(
          'data',
          createCache.bind({
            oldPath: join(dirs.pkgs.web, 'ext'),
            newPath: join(dirs.app.web, 'build', 'web', '__ext'),
          })
        )
        .on('end', () => {
          resolve()
        })
    })
  )
  promises.push(
    new Promise<void>((resolve) => {
      klaw(join(dirs.app.web, 'public'))
        .on(
          'data',
          createCache.bind({
            oldPath: join(dirs.app.web, 'public'),
            newPath: join(dirs.app.web, 'build', 'web'),
          })
        )
        .on('end', () => {
          resolve()
        })
    })
  )
  promises.push(
    new Promise<void>((resolve) => {
      klaw(join(dirs.app.web, 'build', 'web'))
        .on('data', createCache)
        .on('end', () => {
          resolve()
        })
    })
  )

  if (await pathExists(join(dirs.app.web, 'figma', 'imgs'))) {
    promises.push(
      new Promise<void>((resolve) => {
        klaw(join(dirs.app.web, 'figma', 'imgs'))
          .on('data', createCache)
          .on('end', () => {
            resolve()
          })
      })
    )
  }

  await Promise.all(promises)
}
