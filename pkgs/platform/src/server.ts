import { getDMMF } from '@prisma/sdk'
import { dirs, log } from 'boot'
import { Watcher } from 'builder'
import { Metafile } from 'esbuild'
import fastify from 'fastify'
import fastifyCompress from 'fastify-compress'
import fastifyCookie from 'fastify-cookie'
import fastifyEtag from 'fastify-etag'
import fastifyForm from 'fastify-formbody'
import fastifyMultipart from 'fastify-multipart'
import fastifyStatic from 'fastify-static'
import fastifyWS from 'fastify-websocket'
import http from 'http'
import { ellapsedTime } from 'libs'
import fetch from 'node-fetch'
import { join } from 'path'
import sodium from 'sodium-universal'
import * as WebSocket from 'ws'
import { devBuild } from '.'
import { ParentThread } from '../../builder/src/thread'
import { BaseHistory } from './dev/base/history'
import { startDev } from './dev/dev-start'
import { defineRoute } from './routes'
import { cache, reloadCache } from './system/init'
import { authPlugin, jsonPlugin } from './system/middleware'
export type ServerInstance = ReturnType<typeof fastify>
export interface IFigma {
  connected: boolean
  ask: {
    lastId: 0
    answers: {}
    callbacks: Record<string, any>
  }
  saving: Record<string, { jsx: string; nodes: string }>
  ws?: WebSocket
  nextId: number
}

type ThenArg<T> = T extends PromiseLike<infer U> ? U : T
export interface CustomGlobal extends NodeJS.Global {
  mode: 'dev' | 'prod'
  baseUpgrade: {
    mandatory: boolean
    migrating: boolean
  }
  dev: {
    history: BaseHistory
    sync: Watcher
  }
  hasOldCMS: boolean
  host: string
  scheme: string
  port: number
  initialData: any
  shouldExit?: boolean
  metafile?: Metafile
  cache: typeof cache
  build_id: string
  devBuild?: typeof devBuild
  secret: Buffer
  mainjs: string
  serverRoutes: string[]
  figma: IFigma
  fallbackSaving: boolean
  fetch: typeof fetch
  Headers: typeof Headers
  parent: MainControl
  sessionGet: Record<string, any>
  dmmf: ThenArg<ReturnType<typeof getDMMF>>
  fileCaches: Record<string, string>
}
declare const global: CustomGlobal

export type MainControl = {
  signal: (
    module: 'session' | 'platform' | 'web' | 'server' | 'db' | 'mobile',
    data: any | 'restart'
  ) => Promise<any>
  notifyDevBuild: (path: string) => Promise<string>
  onMessage: (msg: any) => Promise<void>
}

export const server = async (
  main: MainControl,
  args: {
    mode: 'dev' | 'prod'
    port: number
    rootstamp: number
    devBuild: typeof devBuild
  },
  parent?: ParentThread
) => {
  const { mode, rootstamp, devBuild } = args
  global.sessionGet = {}
  global.figma = {
    connected: false,
    nextId: 0,
    saving: {},
    ask: {
      lastId: 0,
      callbacks: {},
      answers: {},
    },
  }
  global.parent = main
  global.mode = mode
  global.secret = sodium.sodium_malloc(sodium.crypto_secretbox_KEYBYTES)
  sodium.randombytes_buf(global.secret)
  global.devBuild = devBuild

  await reloadCache()
  if (global.shouldExit) return

  const server =
    mode === 'dev'
      ? fastify({
          bodyLimit: 124857600, // === 100MB
          serverFactory: (handler) => {
            const server = http.createServer((req, res) => {
              handler(req, res)
            })

            return server
          },
        })
      : fastify()

  server.register(fastifyMultipart)
  server.register(fastifyCookie)
  server.register(fastifyForm)
  server.register(fastifyWS)
  server.register(fastifyCompress, { global: false })
  server.register(jsonPlugin)

  server.register(authPlugin)

  server.register(fastifyEtag)
  server.register(fastifyStatic, {
    root: join(dirs.app.web, 'build', 'web'),
    serve: false,
  })

  defineRoute(server, parent)

  if (global.mode === 'dev') {
    startDev(server, parent)
  }

  server.setErrorHandler(function (error, req, reply) {
    const rqh = JSON.stringify(req.headers, null, 2)
      .split('\n')
      .join('\n       ')

    const reh = JSON.stringify(reply.getHeaders(), null, 2)
      .split('\n')
      .join('\n       ')
    log(
      'error',
      `
  URL           : ${req.url} (${reply.statusCode})
  Req Headers   : ${rqh.substr(1, rqh.length - 2)}
  Reply Headers : ${reh.substr(1, reh.length - 2).trim()}
  Stack Trace   :  
  ${
    !!error && !!error.stack
      ? '     ' + error.stack.split('\n').join('\n     ')
      : error
  } 
`
    )
    // Send error response
    reply
      .type('application/json')
      .status(500)
      .send({
        status: 'error',
        code: error.statusCode || 500,
        error:
          !!error && !!error.stack && mode === 'dev'
            ? error.stack.split('\n')
            : error,
      })
  })

  const startServer = (port: number) => {
    return new Promise((resolve: any) => {
      global.host = 'localhost'
      global.port = port
      global.scheme = 'http'

      if (global.shouldExit) return
      server.listen(port, '0.0.0.0', async (err) => {
        if (err) {
          console.error(err)
          process.exit(0)
        }

        log(
          'platform',
          `Ready [${ellapsedTime(rootstamp)}s] http://localhost${
            port === 80 || port === 443 ? `` : `:${port}`
          }`
        )
        resolve()
      })
    })
  }

  main.onMessage = async (msg: any) => {
    if (msg === 'exit') {
      server.close()
      if (global.dev && global.dev.sync) {
        if (global.dev.sync) global.dev.sync.stop()
      }
      return
    }
    switch (msg.action) {
      // case 'dev-build':
      //   const result: BuildResult = get(msg, 'result')
      //   global.devBuild = result
      //   log(
      //     'platform',
      //     `Generated ${
      //       result.outputFiles.length
      //     } in memory files (${ellapsedTime(timestamp)}s)`
      //   )
      //   break
      case 'session-get':
        global.sessionGet[msg.sid] = msg.data
        break
      case 'start':
        await startServer(msg.port)
        if (msg.metafile) {
          global.metafile = msg.metafile
        }
        break
    }
  }
  main.signal('platform', 'server-ready')
}
