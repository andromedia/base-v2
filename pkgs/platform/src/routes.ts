import { dirs } from 'boot'
import crypto from 'crypto'
import fs from 'fs'
import { readdir } from 'fs-extra'
import { join } from 'path'
import { ParentThread } from '../../builder/src/thread'
import { CustomGlobal, ServerInstance } from './server'
import { routeAPI } from './system/route-api'
import { routeComponent } from './system/route-component'
import { routeData } from './system/route-data'
import { routeFigmaImages } from './system/route-figma-images'
import { routeInit } from './system/route-init'
import { routeLayout } from './system/route-layout'
import { routeMain } from './system/route-main'
import { routePage, routePageParams } from './system/route-page'
import { serveStaticFile } from './system/route-static-file'
declare const global: CustomGlobal

export const defineRoute = async (
  server: ServerInstance,
  parent?: ParentThread
) => {
  server.get('/fimgs/*', routeFigmaImages)
  server.get('/__component/:id', routeComponent)
  server.get('/__page/:id.:ext', routePage) 
  server.post('/__params/:id', routePageParams) 
  server.get('/__layout/:id.:ext', routeLayout) 
  server.get('/__init/*', routeInit)          
  server.all('/__data*', async (req, reply) => { 
    routeData(req, reply, global.mode, parent)
  })

  server.all('*', async (req, reply) => {
    if (await routeAPI(req, reply)) {
      return
    } 
   
    let url = req.url.split('?')[0]
    if (await serveStaticFile({ url, req, reply })) {
      return
    }

    await routeMain(req, reply) 
  })
}

export const generateBuildId = async () => {
  if (global.mode === 'dev') {
    // global.build_id = await createHashFromFile(
    //   join(dirs.app.web, 'build', 'web', 'index.js')
    // )
    global.mainjs = '/index.js'
  } else if (global.mode === 'prod') {
    const dir = await readdir(join(dirs.app.web, 'build', 'web'))
    for (let i of dir) {
      if (i.startsWith('main.') && i.endsWith('.js')) {
        global.build_id = i.split('.')[1]
        global.mainjs = `/${i}`
      }
    }
  }
}
export const createHashFromFile = (filePath) =>
  new Promise<string>((resolve) => {
    const hash = crypto.createHash('sha1')
    fs.createReadStream(filePath)
      .on('data', (data) => hash.update(data))
      .on('end', () => resolve(hash.digest('hex')))
  })
