import { dirs } from 'boot'
import { BuilderPool } from 'builder'
import { ParentThread } from 'builder/src/thread'
import { BuildResult, Metafile } from 'esbuild'
import { pathExists, readFile, writeFile } from 'fs-extra'
import { find } from 'lodash'
import { join } from 'path'
import { buildDB } from './start/build-db'
import { buildLibs } from './start/build-libs'
import { buildPlatform } from './start/build-platform'
import { buildServer } from './start/build-server'
import { buildWebDev } from './start/build-web-dev'
import { buildWebProd } from './start/build-web-prod'

export interface MainGlobal extends NodeJS.Global {
  mode: 'dev' | 'prod'
  parent: ParentThread
  rootstamp: number
  mainstamp: number
  port: number
  platform: {
    ready: boolean
    metafile?: Metafile
  }
  pool: BuilderPool
  devBuild?: BuildResult
  entryPoints: Record<string, string>
  twcwd: string
  twconf: string
}

declare const global: MainGlobal

export const start = async (
  port: number,
  mode: 'dev' | 'prod',
  parent: ParentThread
) => {
  const pool = new BuilderPool()
  global.mainstamp = new Date().getTime()
  global.pool = pool
  global.port = port
  global.platform = {
    ready: false,
    metafile: null,
  }

  if (mode === 'dev') {
    const uipath = join(dirs.pkgs.figma, 'bin', 'ui.html')
    if (await pathExists(uipath)) {
      const src = (await readFile(uipath, 'utf-8')).replace(
        '[url]',
        `http://localhost:${port}`
      )
      await writeFile(uipath, src)
    }

    await writeFile(
      join(dirs.pkgs.figma, 'src', 'host.js'),
      `module.exports = 'localhost:${port}';`
    )
  }

  await buildLibs(pool, mode)
  await buildServer(pool, mode)
  await buildDB(pool)

  if (mode === 'dev') await buildWebDev(pool, mode)
  else await buildWebProd(pool, mode)

  await buildPlatform(pool, mode)
  parent.notify('ready')

  pool.onParentMessage(async (msg) => {
    if (typeof msg === 'object' && msg.type === 'platform-signal') {
      if (msg.action === 'dev-build') {
        const path = join(dirs.app.web, 'build', 'web', msg.path)
        const file = find(global.devBuild.outputFiles, { path })
        console.log(
          file,
          global.devBuild.outputFiles
            .filter((e) => e.path.indexOf('index') >= 0)
            .map((e) => e.path),
          path
        )
        return ''
      }

      if (msg.module === 'session') {
        const data = msg.data
        if (data.action === 'session-set') {
        } else if (data.action === 'session-get') {
          pool.send('platform', {
            action: 'session-get',
            sid: data.sid,
            data: {},
          })
        } else if (data.action === 'session-del') {
        }
      }

      switch (msg.data) {
        case 'rebuild-db':
          await pool.rebuild('db')
          break
        case 'restart':
          await pool.rebuild('platform')
          break
        case 'server-ready':
          global.platform.ready = true
          await pool.send('platform', {
            action: 'start',
            port,
            metafile: global.platform.metafile,
          })
          break
      }
    }
  })
}
