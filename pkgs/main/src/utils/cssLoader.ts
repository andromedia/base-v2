import autoprefixer from 'autoprefixer'
import { dirs, log } from 'boot'
import { ensureDir } from 'fs-extra'
import { readFile, writeFile } from 'fs/promises'
import { ellapsedTime } from '../../../libs/src/ellapsed-time'
import padEnd from 'lodash.padend'
import trim from 'lodash.trim'
import { basename, dirname, join } from 'path'
import postcss from 'postcss'
import tailwindcss from 'tailwindcss'
import { MainGlobal } from '../start'
export const cssFilesBuilt: any = {}

declare const global: MainGlobal

export const cssLoader = (mode: 'dev' | 'prod') => ({
  name: 'css-loader',
  setup: async function (build: any) {
    if (!global.twconf) {
      global.twconf = await readFile(
        join(dirs.app.web, 'tailwind.config.js'),
        'utf-8'
      )
    }

    build.onResolve({ filter: /^loadStyle$/ }, () => {
      return { path: 'loadStyle', namespace: 'loadStyleShim' }
    })
    build.onLoad({ filter: /^loadStyle$/, namespace: 'loadStyleShim' }, () => {
      return {
        contents: `export function loadStyle(href) {  
        return new Promise(function (resolve, reject) {
          const ex = document.querySelector(\`link[href="\${href}?${new Date().getTime()}"]\`);
          if (ex) ex.remove();
      
          let link = document.createElement("link");
          link.href = href;
          link.rel = "stylesheet";
      
          link.onload = () => resolve(link);
          link.onerror = () => reject(new Error(\`Style load error for \${href}\`));
      
          document.head.append(link);
        });
      }`,
      }
    })

    build.onLoad({ filter: /\.s?css$/ }, async (args: any) => {
      const time = new Date().getTime()
      cssFilesBuilt[args.path] = await buildCss(args.path, mode)

      if (global.mode === 'dev') {
        if (new Date().getTime() - time > 1500)
          process.stdout.write(
            ` ${basename(args.path)} [${ellapsedTime(time)}s]`
          )
      } else {
        log('web', `built: ${padEnd(basename(args.path), 30, '.')} ${time}.`)
      }

      return {
        contents:
          args.path.indexOf('index.css') > 0
            ? ''
            : `
import {loadStyle} from 'loadStyle'
loadStyle(${cssFilesBuilt[args.path]})
        `.trim(),
        loader: 'js',
      }
    })
  },
})

let first = true

const buildCss = (from: any, mode: 'dev' | 'prod') => {
  return new Promise(async (resolve) => {
    const srcpath = join(dirs.root, 'app', 'web', 'src')
    const nodepath = join(dirs.root, 'node_modules')
    const buildpath = join(dirs.app.web, 'build', 'web')
    let topath = ''

    if (from.indexOf(srcpath) === 0) {
      topath =
        '/' + trim(from.substr(srcpath.length + 1).replace(/\\/g, '/'), '/', {})
    } else if (from.indexOf(nodepath) === 0) {
      topath =
        '/node/' +
        trim(from.substr(nodepath.length + 1).replace(/\\/g, '/'), '/', {})
    } else if (from.indexOf(dirs.pkgs.web) === 0) {
      topath =
        '/pkgs/web/' +
        trim(from.substr(dirs.pkgs.web.length + 1).replace(/\\/g, '/'), '/', {})
    }

    const fkey = from.substr(dirs.app.web.length)

    const to = join(buildpath, topath)
    const css = await readFile(from, 'utf-8')
    const tailwindcfg = join(dirs.app.web, 'tailwind.config.js')

    const tailwind = {
      ...require(tailwindcfg),
      mode: 'jit',
      purge: {
        enabled: true,
        content: [
          join(dirs.app.web, 'src', '**/*.tsx'),
          join(dirs.app.web, 'src', '**/*.jsx'),
          join(dirs.pkgs.web, '**/*.tsx'),
          join(dirs.app.web, 'cms', '**/*.html'),
        ],
      },
    }
    const process = await postcss([
      autoprefixer,
      tailwindcss(tailwind),
      require('cssnano')({
        preset: 'default',
      }),
    ]).process(css, {
      from,
      to,
    })
    const result = process.css
    await ensureDir(dirname(to))
    await writeFile(to, result)
    if (first) {
      log('boot', 'Tailwind CSS', false)
      first = false
    }

    resolve(JSON.stringify(topath))
  })
}
