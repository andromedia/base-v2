import { dirs, log } from 'boot'
import { BuilderPool } from 'builder'
import { BuildResult } from 'esbuild'
import { pathExists, readJSON, writeJSON } from 'fs-extra'
import { padStart } from 'lodash'
import { join } from 'path'
import { ellapsedTime } from '../../../libs/src/ellapsed-time'
import { waitUntil } from '../../../libs/src/wait-until'
import { MainGlobal } from '../start'
import { ensureMain } from '../utils/ensureMain'

declare const global: MainGlobal

export const buildPlatform = async (
  pool: BuilderPool,
  mode: 'dev' | 'prod'
) => {
  if (mode === 'prod') {
    log('platform', 'Building', false)
  }

  process.stdout.write(' • Platform')

  const appDeps = {
    db: '1.0.0',
    server: '1.0.0',
  }

  if (
    (await pathExists(join(dirs.root, 'app'))) &&
    (await pathExists(join(dirs.pkgs.platform, 'package.json')))
  ) {
    const pkgPath = join(dirs.pkgs.platform, 'package.json')
    const json = await readJSON(pkgPath)
    let shouldWrite = false
    for (const [k, v] of Object.entries(appDeps)) {
      if (!json.dependencies[k]) {
        json.dependencies[k] = v
        shouldWrite = true
      }
    }

    if (shouldWrite) {
      await writeJSON(pkgPath, json, {
        spaces: 2,
      })
    }
  }

  let initialBuild = true

  await pool.add('platform', {
    root: dirs.pkgs.platform,
    in: join(dirs.pkgs.platform, 'src', 'index.ts'),
    out: join(dirs.pkgs.platform, 'build', 'index.js'),
    buildOptions: {
      metafile: true,
    },
    onChange: async (path) => {
      process.stdout.write('\n')
      const readline = require('readline')
      const blank = '\n'.repeat(process.stdout.rows)
      console.log(blank)
      readline.cursorTo(process.stdout, 0, 0)
      readline.clearScreenDown(process.stdout)
      log('boot', 'Development • Restarting Web Server')
      global.rootstamp = new Date().getTime()
      await pool.rebuild('platform')
      initialBuild = false
    },
    onBuilt: async () => {
      await ensureMain(dirs.pkgs.platform)
      if (initialBuild)
        process.stdout.write(` • [${ellapsedTime(global.mainstamp)}s]\n`)

      pool.run('platform', {
        mode: global.mode,
        port: global.port,
        rootstamp: global.rootstamp,
      })

      await waitUntil(() => pool.running['platform'] && global.devBuild)
      await sendDevOutputFiles(pool, global.devBuild)
    },
  })
}

export const sendDevOutputFiles = async (
  pool: BuilderPool,
  result: BuildResult,
  rebuilding?: boolean
) => {
  const outputFiles: any[] = Object.values(result.outputFiles)
  if (global.mode === 'dev') {
    if (rebuilding) {
      log('base', `Rebuilding `, false)
    } else {
      log('base', `Generating ${outputFiles.length} files `, false)
    }
  }

  pool.running['platform'].thread.postMessage(
    `devb!0000${new Date().getTime()}`
  )

  for (let e of outputFiles) {
    pool.running['platform'].thread.postMessage(
      `devb!${padStart(e.path.length + '', 4, '0')}${e.path}${e.text}`
    )
  }

  pool.running['platform'].thread.postMessage(`devb!9999`)

  global.devBuild = result
}
