import { dirs, log } from 'boot'
import { BuilderPool } from 'builder'
import { pathExists, readJSON, remove } from 'fs-extra'
import { join } from 'path'
import { waitUntil } from '../../../libs/src/wait-until'
import { MainGlobal } from '../start'
import { cssLoader } from '../utils/cssLoader'
import { webFiles, webPkgs } from '../utils/devFiles'
import { ensureProject } from '../utils/ensureProject'
import { overrideWebIndex } from '../utils/overrideWebIndex'
import { runYarn } from '../utils/yarn'
import { sendDevOutputFiles } from './build-platform'

declare const global: MainGlobal

export const buildWebDev = async (
  pool: BuilderPool,
  mode: 'dev' | 'prod'
) => {
  if (!(await pathExists(join(dirs.app.web, 'disabled')))) {
    process.stdout.write(' • Web')

    await remove(join(dirs.app.web, 'build', 'web'))

    if (
      await ensureProject('web-app', dirs.app.web, {
        files: webFiles,
        pkgs: webPkgs,
      })
    ) {
      await runYarn('install')
    }

    if (await pathExists(join(dirs.app.web, 'build', 'deps.json'))) {
      await readJSON(join(dirs.app.web, 'build', 'deps.json'))
    }

    const ins = [join(dirs.app.web, 'src', 'index.tsx')]
    await pool.add('web', {
      root: dirs.app.web,
      platform: 'browser',
      in: ins,
      buildOptions: {
        incremental: true,
        chunkNames: 'chunks/[name]-[hash]',
        minify: true,
        pure: ['getNodeInfo'],
        sourcemap: true,
        splitting: true,
        keepNames: true,
        metafile: true,
        write: false,
        treeShaking: true,
        plugins: [cssLoader('dev')],
        outdir: join(dirs.app.web, 'build', 'web'),
        external: [],
        target: 'es6',
      },
      plugins: [cssLoader(mode)],
      onChange: async (ev, path, builder) => {
        if (global.platform.ready) {
          log('refresh', `🏗️  ${path}`, false)
          pool.send('platform', 'notify-refresh-all')

          try {
            const built = await pool.rebuild('web')
            process.stdout.write(' [DONE]\n')

            await sendDevOutputFiles(pool, built, true)
          } catch (e) {
            process.stdout.write(` [FAILED]\n${e}\n\n`)
          }
        }
      },
      onBuilt: async (result) => {
        await waitUntil(() => {
          return pool.status('platform') === 'running'
        })
        global.devBuild = result

        if (!global.platform.ready) {
          await overrideWebIndex(mode)
        }
      },
    })
  }
}
