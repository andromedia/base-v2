/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import { Label, Spinner, SpinnerSize } from '@fluentui/react'
import { waitUntil } from 'libs'
import { createContext, useEffect } from 'react'
import { BaseWindow } from 'web.init/src/window'
import { useRender } from 'web.utils/src/useRender'
import { _figma } from './Figma'
import { init } from './figma/init'
import { Code } from './internal/Code'
import { CodeFooter } from './internal/CodeFooter'
import { Resizer } from './internal/Resizer'
import { TopBar } from './internal/TopBar'
declare const window: BaseWindow

const FigmaContext = createContext({} as typeof _figma)
FigmaContext.displayName = 'Figma'

export default () => {
  const render = useRender()

  useEffect(() => {
    init()

    // setTimeout(() => {
    //   askFigma(() => {
    //     const a: any = figma.getNodeById('3316:7885')
    //     if (a) {
    //       let page = a
    //       while (page.type !== 'PAGE') {
    //         page = page.parent
    //       }
    //       figma.currentPage = page;
    //       figma.currentPage.selection = [a]
    //     }
    //   })
    // }, 1000)
  }, [])

  useEffect(() => {
    const keydown = function (e) {
      if (
        (window.navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey) &&
        e.keyCode == 83
      ) {
        e.preventDefault()
        _figma.current.save()
      }
    }
    document.addEventListener('keydown', keydown, false)

    return () => {
      document.removeEventListener('keydown', keydown)
    }
  }, [])

  _figma.main.render = render
  _figma.main.relayout = async () => {
    _figma.main.init = false
    render()

    await waitUntil(() => 1000)

    _figma.main.init = true
    render()
  }

  return (
    <FigmaContext.Provider value={_figma}>
      <div
        className="flex absolute inset-0 flex-col"
        css={css`
          .error {
            background: red;
            label {
              color: white !important;
              &.active {
                color: black !important;
              }
            }
          }
        `}
      >
        <Resizer>
          {_figma.main.init ? (
            <>
              <div className="flex flex-1 flex-col">
                <TopBar />
                <div
                  className="flex flex-1 relative"
                  css={css`
                    padding-bottom: 22px;
                  `}
                >
                  <FigmaContext.Consumer>
                    {(value) => <Code _fig={value} />}
                  </FigmaContext.Consumer>

                  {!_figma.main.connected ? (
                    <div className="fixed z-10 inset-0 bg-white bg-opacity-50 flex items-center justify-center flex-col">
                      <Spinner size={SpinnerSize.large} />
                      <Label>Connecting</Label>
                    </div>
                  ) : (
                    <div
                      className={`fixed z-10 inset-0 bg-white bg-opacity-80 flex items-center justify-center flex-col transition-all ${
                        _figma.current.nodeIsLoading
                          ? 'opacity-1'
                          : 'opacity-0 pointer-events-none '
                      }`}
                    >
                      <Spinner size={SpinnerSize.large} />
                      <Label className="text-center">
                        Loading Node
                        <br />
                        {_figma.current.nextNodeName}
                      </Label>
                    </div>
                  )}
                </div>
                <CodeFooter />
              </div>
            </>
          ) : (
            <Label className="flex flex-1 items-center justify-center">
              Loading...
            </Label>
          )}
        </Resizer>
      </div>
    </FigmaContext.Provider>
  )
}
