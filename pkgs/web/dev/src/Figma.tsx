/** @jsx jsx */
import type { Position } from 'monaco-editor'
import { BaseWindow } from 'web.init/src/window'
import { FigmaBase } from './figma/figma-base'
import { FigmaFrame, FigmaNode } from './figma/figma-node'
import { FigmaPage } from './figma/figma-page'
declare const window: BaseWindow

export const _figma = {
  current: {
    page: null as null | FigmaPage,
    mode: 'node' as 'node' | 'frame' | 'effect',
    node: null as null | FigmaNode,
    frameTree: null as null | FigmaFrame,
    nextNodeName: '',
    nodeIsLoading: false,
    refreshNode: async () => {},
    save: async () => {},
    saving: false,
    code: {
      expandChild: localStorage.getItem('figma-base-expand-child') === 'y',
      focusLoc: (loc: { line: number; column: number }) => {},
      triggerFold: () => {},
      refreshing: false,
      refreshInterval: 0 as ReturnType<typeof setInterval>,
      startAutoRefresh: () => {},
      stopAutoRefresh: () => {},
      lastPosition: null as null | Position,
      parseRestriction: (text: string) => {},
      fontSize: 13,
    },
  },
  tools: {
    fetchNodeMap: (id: string): Promise<Record<string, any>> => ({} as any),
    recursivePrintHtml: (node: FigmaNode): string => '',
    fetchNode: (id: string, useCache: boolean = true): Promise<FigmaNode> =>
      ({} as any),
    fetchFrame: (id: string, useCache: boolean = true): Promise<FigmaFrame> =>
      ({} as any),
  },
  main: {
    init: false,
    connected: false,
    render: () => {},
    renderTopBar: () => {},
    renderResizer: () => {},
    relayout: () => {},
    renderFooter: () => {},
  },
  cache: {
    pages: {} as Record<string, FigmaPage>,
    framesByTargetId: {} as Record<string, FigmaFrame>,
    base: null as null | FigmaBase,
  },
}
