/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import { memo, useEffect, useRef } from 'react'
import { askFigma } from 'web.dev/src/figma/ask-figma'
import { askPlatform } from 'web.dev/src/libs/ask-platform'
import { BaseWindow } from 'web.init/src/window'
import { useRender } from 'web.utils/src/useRender'
import { PureSelect } from '../../../crud/src/form/web/fields/WSelect'
import { _figma } from '../Figma'
declare const window: BaseWindow

export const CodeFooter = memo(() => {
  const _ = useRef({
    ready: false,
    pages: [] as { id: string; name: string; layout_id: string; url: string }[],
    layouts: [] as {
      id: string
      name: string
    }[],
  })
  const meta = _.current
  const render = useRender()

  useEffect(() => {
    ;(async () => {
      meta.pages = (await askPlatform((x) => {
        return Object.values(x.global.cache.page).map((e) => ({
          id: e.id,
          name: e.name,
          layout_id: e.layout_id,
          url: e.url,
        }))
      })) as any
      meta.layouts = (await askPlatform((x) => {
        return Object.values(x.global.cache.layout).map((e) => ({
          id: e.id,
          name: e.name,
        }))
      })) as any
      meta.ready = true
      render()
    })()
  }, [])

  const ft = _figma.current.frameTree
  if (!meta.ready) return null
  _figma.main.renderFooter = render

  let page = null as any
  for (let i of meta.pages) {
    if (i.id === ft?.target.id) {
      page = i
      break
    }
  }
  return (
    <div
      className="footer flex justify-between items-center border-r border-l border-gray-300"
      css={css`
        position: absolute;
        bottom: 0px;
        left: 0px;
        right: 0px;
        width: 100%;
        height: 22px;
        border-top: 1px solid #ececeb;

        overflow: hidden;
        label {
          font-size: 12px;
          background: transparent !important;
        }

        .pure-select {
          height: 22px;
          > div {
            margin: 0px 5px;
          }
          i {
            display: none;
          }
          & > .absolute > .ms-Label {
            padding: 0px;
            margin: 0px -2px;
            height: 22px;
            line-height: 15px;
            font-size: 12px;
          }
          .ms-TextField {
            height: 22px;
            margin-bottom: 10px;
            input,
            .ms-TextField-fieldGroup,
            .ms-TextField-wrapper {
              background: transparent !important;
              font-weight: 600;
              border: 0px;
              height: 22px;
              line-height: 20px;
              font-size: 12px;
              margin: 0px;
              padding: 0px;
            }
            .ms-TextField-fieldGroup::after {
              display: none;
            }
          }
        }
      `}
    >
      <PureSelect
        value={ft?.target.id || '00000'}
        items={meta.pages.map((e) => ({
          value: e.id,
          label: `${e.id} | ${e.name}`,
          el: (
            <div className="flex-1 flex flex-row items-center">
              <div
                className="w-12 text-center rounded mr-1 bg-blue-100 "
                css={css`
                  font-size: 10px;
                `}
              >
                {e.id}
              </div>{' '}
              {e.name}
            </div>
          ),
        }))}
        onChange={(e) => {
          const frame = _figma.cache.framesByTargetId[e]
          console.log(e, frame)
          askFigma(
            (x) => {
              const frame = figma.getNodeById(x.id) as FrameNode
              if (frame && frame.parent && frame.parent.type === 'PAGE') {
                figma.currentPage = frame.parent
                figma.currentPage.selection = [frame]
              }
            },
            { id: frame.id }
          )
        }}
      />
      <PureSelect
        css={css`
          flex-basis: 35%;
          flex-grow: unset;
          input {
            text-align: right;
          }
        `}
        value={page?.layout_id || '00000'}
        items={meta.layouts.map((e) => ({
          value: e.id,
          label: e.name,
        }))}
        onChange={(e) => {
          const ft = _figma.current.frameTree

          console.log(ft, _figma.cache.pages)
        }}
      />
    </div>
  )
})
