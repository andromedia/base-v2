/** @jsx jsx */
import { Node } from '@babel/types'
import { css, jsx } from '@emotion/react'
import { Label } from '@fluentui/react'
import { waitUntil } from 'libs'
import type { Range } from 'monaco-editor'
import { useRef } from 'react'
import { BaseWindow } from 'web.init/src/window'
import { useRender } from 'web.utils/src/useRender'
import { _figma } from '../Figma'
import { askFigma } from '../figma/ask-figma'
import { recursivePrintHtml } from '../figma/figma-node'
import { CodeFooter } from './CodeFooter'
import { TemplateCode } from './TemplateCode'
import throttle from 'lodash.throttle'
declare const window: BaseWindow

export const Code = ({ _fig }: { _fig: typeof _figma }) => {
  const current = _fig.current
  if (!current.node) {
    return (
      <div className="flex flex-col flex-1 self-stretch border-r border-l border-gray-300">
        <Label className="flex-1 flex items-center justify-center">
          Please select something...
        </Label>
      </div>
    )
  }

  let node = current.node
  const frameTree = current.frameTree
  const _ = useRef({
    editor: null as any,
    value: '',
    refreshInterval: null as null | ReturnType<typeof setInterval>,
    restricted: [] as Range[],
    ast: {} as Record<string, Node>,
    linkedTags: [] as { open: Range; close: Range }[],
  })
  const meta = _.current
  const render = useRender()

  const format = window.babel.prettier
  if (!format) {
    waitUntil(() => window.babel.prettier).then(render)
    return null
  }

  if (frameTree) {
    if (current.mode === 'frame') {
      node = frameTree
    }

    if (frameTree.error) {
      if (typeof frameTree.error === 'string') {
        frameTree.error = JSON.parse(frameTree.error)
      }

      if (typeof frameTree.error === 'string') {
        frameTree.error = JSON.parse(frameTree.error)
      }
      if (frameTree.error) meta.value = frameTree.error.code
    } else {
      if (current.mode === 'effect') {
        if (frameTree.html.effect) {
          meta.value = format(frameTree.html.effect)
        }
      } else {
        if (_fig.current.code.expandChild) {
          meta.value = format(
            recursivePrintHtml(node, { figmaReactElements: false })
          )
        } else if (node.html) {
          meta.value = format(node.html.code)
        } else {
          console.log(node)
        }
      }
    }
  }

  setTimeout(async () => {
    try {
      await waitUntil(() => meta.editor)
      _figma.current.code.parseRestriction(meta.value)
    } catch (e: any) {
      meta.editor.deltaDecorations([], [])

      const frameTree = _figma.current.frameTree
      if (frameTree) {
        await askFigma(
          (x) => {
            const node = figma.getNodeById(x.id)
            if (node) {
              node.setPluginData('f-error', JSON.stringify(x.error))
            }
          },
          {
            id: frameTree.id,
            error: {
              node: _figma.current.node,
              msg: e.message,
              loc: e.loc,
              code: meta.value,
            },
          }
        )

        _figma.main.renderTopBar()
        _figma.main.renderResizer()
      }
      console.warn(`Failed to parse fnode restriction:\n`, e)
    }
  })

  const startAutoRefresh = () => {
    clearInterval(_figma.current.code.refreshInterval)
    _figma.current.code.refreshInterval = setInterval(() => {
      const ft = _figma.current.frameTree

      if (ft && !ft.error) _figma.current.refreshNode()
    }, 1000)
  }
  const stopAutoRefresh = () => {
    clearInterval(_figma.current.code.refreshInterval)
  }

  return (
    <div
      className="flex flex-1 flex-col items-stretch border-r border-l border-gray-300"
      css={css`
        .fnode {
          color: #aaa !important;
          cursor: pointer;
        }
        .folded-background {
          background: transparent !important;
        }
        .linked-editing-decoration {
          background: rgba(0, 0, 0, 0.1) !important;
        }
      `}
    >
      <TemplateCode
        value={meta.value}
        onMount={(editor, monaco) => {
          meta.editor = editor
          _figma.current.code.focusLoc = (loc) => {
            editor.revealLineInCenter(loc.line)
            editor.setPosition({ column: loc.column, lineNumber: loc.line })
          }
          startAutoRefresh()

          editor.onDidBlurEditorWidget(() => {
            startAutoRefresh()
            _figma.current.code.lastPosition = editor.getPosition()
          })

          editor.onDidFocusEditorWidget(() => {
            stopAutoRefresh()

            if (_figma.current.code.lastPosition) {
              editor.revealLine(_figma.current.code.lastPosition.lineNumber)
              editor.setPosition(_figma.current.code.lastPosition)
            }
          })

          monaco.languages.registerLinkedEditingRangeProvider('typescript', {
            provideLinkedEditingRanges: async (doc, pos, cancel) => {
              for (let i of meta.linkedTags) {
                if (
                  i.close.containsPosition(pos) ||
                  i.open.containsPosition(pos)
                ) {
                  return {
                    ranges: [i.open, i.close],
                    wordPattern: /^[a-z0-9_-]+$/i,
                  }
                }
              }
              return {}
            },
          })
          _figma.current.code.parseRestriction = (text: string) => {
            const babel = window.babel
            if (babel.parse && babel.traverse) {
              meta.linkedTags = []

              const parsed = babel.parse(text, {
                sourceType: 'module',
                plugins: ['jsx', 'typescript'],
              })

              meta.ast = {}
              meta.restricted = []

              babel.traverse(parsed, {
                enter: (path) => {
                  const c = path.node
                  if (c.type === 'JSXFragment') {
                    const open = c.openingFragment.loc
                    if (open) {
                      open.start.column++
                      open.end.column = open.end.column - 1
                    }

                    const close = c.closingFragment.loc
                    if (close) {
                      close.start.column += 2
                      close.end.column = close.start.column
                    }
                    meta.linkedTags.push({
                      open: toMonacoRange(open, monaco),
                      close: toMonacoRange(close, monaco),
                    })
                    return
                  }

                  if (c.type === 'JSXElement') {
                    if (
                      c.openingElement.name.type === 'JSXIdentifier' &&
                      c.openingElement.name.name === 'fnode'
                    ) {
                      const range = toMonacoRange(c.openingElement.loc, monaco)
                      ;(range as any).type = 'open-tag'
                      meta.restricted.push(range)

                      // store ast location
                      // so we can reverse lookup the code ast from node
                      let id = ''
                      for (let e of c.openingElement.attributes) {
                        if (
                          e.type === 'JSXAttribute' &&
                          e.name.name === 'id' &&
                          e.value?.type === 'StringLiteral'
                        ) {
                          id = e.value.value
                        }
                      }
                      if (id) meta.ast[id] = c
                    }

                    if (
                      c.closingElement &&
                      c.closingElement.name.type === 'JSXIdentifier'
                    ) {
                      if (c.closingElement.name.name === 'fnode') {
                        const range = toMonacoRange(
                          c.closingElement.loc,
                          monaco
                        )
                        ;(range as any).type = 'close-tag'
                        meta.restricted.push(range)
                      } else {
                        meta.linkedTags.push({
                          open: toMonacoRange(
                            c.openingElement.name.loc,
                            monaco
                          ),
                          close: toMonacoRange(
                            c.closingElement.name.loc,
                            monaco
                          ),
                        })
                      }
                    }
                  }
                },
              })
            }
            editor.deltaDecorations(
              [],
              meta.restricted.map((range) => ({
                range,
                options: { inlineClassName: 'fnode' },
              }))
            )
          }

          _figma.current.code.triggerFold = () => {
            const foldingContrib = editor.getContribution(
              'editor.contrib.folding'
            )
            foldingContrib.getFoldingModel().then((foldingModel) => {
              const toFold: any[] = []
              for (let i of meta.restricted) {
                if ((i as any).type === 'open-tag') {
                  toFold.push(foldingModel.getRegionAtLine(i.startLineNumber))
                }
              }
              foldingModel.toggleCollapseState(toFold)
            })
          }

          editor.onDidChangeModelContent((e) => {
            if (editor.isUndoingRestriction) {
              editor.isUndoingRestriction = false
              return
            }

            for (const ch of e.changes) {
              const cur = _figma.current
              const frameTree = cur.frameTree
              if (frameTree && cur.node) {
                const value = editor.getModel().getValue()

                if (current.mode === 'effect') {
                  askFigma(
                    (x) => {
                      const node = figma.getNodeById(x.id)
                      if (node) {
                        node.setPluginData('effect', x.value)
                      }
                    },
                    { id: frameTree.id, value }
                  )
                  frameTree.html.effect = value
                } else {
                  let isRestricted = false
                  if (meta.restricted.length > 0) {
                    for (let i of meta.restricted) {
                      if (i.containsRange(ch.range)) {
                        isRestricted = true
                      }
                    }
                  }
                  if (isRestricted) {
                    editor.isUndoingRestriction = true
                    editor.getModel().undo()
                    return
                  }

                  try {
                    _figma.current.code.parseRestriction(value)

                    if (_figma.cache.base && _figma.current.node) {
                      _figma.cache.base.syncHtmlToFigma(_figma.current.node, {
                        astFigmaNodeMap: meta.ast,
                        changeOffset: ch.rangeOffset,
                      })
                    }
                  } catch (e: any) {
                    meta.editor.deltaDecorations([], [])
                    frameTree.error = {
                      code: value,
                      loc: e.loc,
                      msg: e.message,
                      node: cur.node,
                    }
                    askFigma(
                      (x) => {
                        const node = figma.getNodeById(x.id)
                        if (node) {
                          node.setPluginData('f-error', JSON.stringify(x.error))
                        }
                      },
                      {
                        id: frameTree.id,
                        error: frameTree.error,
                      }
                    )
                    _figma.main.renderTopBar()
                    _figma.main.renderResizer()

                    console.warn(
                      '[FigmaBase] Failed to parse code:\n',
                      e.message
                    )
                    return
                  }
                  if (frameTree.error) {
                    frameTree.error = null

                    askFigma(
                      (x) => {
                        const node = figma.getNodeById(x.id)
                        if (node) {
                          node.setPluginData('f-error', '')
                        }
                      },
                      {
                        id: frameTree.id,
                      }
                    )
                    _figma.main.renderTopBar()
                    _figma.main.renderResizer()
                  }

                  return
                }
              }
            }
          })
        }}
        options={{
          wordWrap: 'on',
          glyphMargin: false,
          tabSize: 2,
          minimap: { maxColumn: 50, size: 'fit' },
          fontSize: _figma.current.code.fontSize,
          lineNumbers: 'on',
          folding: true,
          overviewRulerLanes: 0,
          scrollbar: {
            vertical: 'auto',
            verticalScrollbarSize: 3,
            // arrowSize: 10,
            horizontal: 'hidden',
          },
          suggest: {
            showFiles: false,
          },
          smoothScrolling: true,
          lineDecorationsWidth: 10,
          lineNumbersMinChars: 3,
        }}
      />
    </div>
  )
}

const toMonacoRange = (loc, monaco) => {
  if (!loc || !loc.start) {
    return new monaco.Range(1, 1, 1, 1)
  }
  return new monaco.Range(
    loc.start.line,
    loc.start.column + 1,
    loc.end ? loc.end.line : loc.start.line,
    loc.end ? loc.end.column + 1 : loc.start.column + 1
  ) as Range
}
