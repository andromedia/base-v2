
/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import { DefaultButton, Label } from '@fluentui/react'
import get from 'lodash.get'
import { BaseWindow } from 'web.init/src/window'
import { useRender } from 'web.utils/src/useRender'
import { _figma } from '../Figma'
import { askFigma } from '../figma/ask-figma'
import { getFigmaNodeById } from '../figma/figma-base'
import { FigmaFrame, FigmaNode } from '../figma/figma-node'
declare const window: BaseWindow

export const TopBar = () => {
  const current = _figma.current
  const error: FigmaFrame['error'] = get(current, 'frameTree.error')
  const render = useRender()
  _figma.main.renderTopBar = render
  return (
    <div
      className={`flex flex-row border-b border-gray-300 justify-between items-center topbar bg-white`}
      css={css`

        label,
        .ms-Button {
          line-height: 20px;
          min-width: 0px;
          padding: 0px 5px;
          cursor: default;
          font-size: 12px;
          font-weight: 700;
          color: #aaa;

          border: 1px solid transparent;
          &.active,
          :hover {
            color: #333;
          }

          &.active {
            border-color: #ccc;
            background: white;
            border-bottom: 0px;
          }
        }

        .ms-Button {
          margin-top: 1px !important;
          height: 18px !important;
          border: 1px solid #ccc;
        }
        label {
          height: 22px;
        }
      `}
    >
      {error ? (
        <Label
          css={css`
            background: rgba(255, 255, 255, 0.2);
            cursor: pointer !important;

            @keyframes color {
              0% {
                background-color: rgba(255, 255, 255, 0.2);
              }
              50% {
                background-color: rgba(255, 255, 255, 0.5);
              }
              100% {
                background-color: rgba(255, 255, 255, 0.2);
              }
            }

            animation-name: color;
            animation-duration: 2s;
            animation-iteration-count: infinite;
          `}
          onClick={() => {
            _figma.current.code.focusLoc(error.loc)
          }}
        >
          ERROR: {error.msg}
        </Label>
      ) : (
        <>
          <div
            className="flex-row flex"
            css={css`
              margin-bottom: -1px;
            `}
          >
            <Label
              onClick={() => {
                _figma.current.mode = 'effect'
                _figma.main.render()
              }}
              className={`${_figma.current.mode === 'effect' ? 'active' : ''}`}
            >
              Effect
            </Label>

            {current.node && current.node.id !== current.frameTree?.id ? (
              <>
                <Label
                  onClick={() => {
                    _figma.current.mode = 'frame'
                    _figma.main.render()
                  }}
                  css={css`
                    &.not-active {
                      border-left: 1px solid #ececeb !important;
                    }
                  `}
                  className={`${
                    _figma.current.mode === 'frame' ? 'active' : 'not-active'
                  }`}
                >
                  Root
                </Label>
                <Label
                  onClick={() => {
                    _figma.current.mode = 'node'
                    _figma.main.render()
                  }}
                  className={`${
                    _figma.current.mode === 'node' ? 'active' : ''
                  }`}
                >
                  Node
                </Label>

                {_figma.current.node &&
                  _figma.current.node.parent &&
                  _figma.current.mode === 'node' && (
                    <DefaultButton
                      css={css`
                        margin-left: 2px !important;
                        height: 18px !important;
                        i {
                          font-size: 10px;
                          color: #333 !important;
                          font-weight: 700 !important;
                        }
                      `}
                      onClick={() => {
                        let node = _figma.current.node
                        if (node && node.parent) {
                          let parent: FigmaNode | null | string = node.parent
                          while (
                            typeof parent === 'string' ||
                            (typeof parent === 'object' &&
                              parent &&
                              !parent.html)
                          ) {
                            if (typeof parent === 'string') {
                              parent = get(
                                _figma.current.frameTree,
                                parent.substr(6)
                              )
                            } else if (
                              typeof parent === 'object' &&
                              parent &&
                              parent.id &&
                              _figma.current.frameTree
                            ) {
                              parent = getFigmaNodeById(
                                parent.id,
                                _figma.current.frameTree
                              )
                            } else {
                              break
                            }
                          }
                          if (parent) {
                            _figma.current.node = parent
                          } else {
                            _figma.current.node = _figma.current.frameTree
                          }
                          node = _figma.current.node
                          if (node)
                            askFigma(
                              (x) => {
                                const node = figma.getNodeById(x.id)
                                if (node)
                                  figma.currentPage.selection = [
                                    node as SceneNode,
                                  ]
                              },
                              { id: node.id }
                            )

                          _figma.main.relayout()
                        }
                      }}
                      iconProps={{ iconName: 'ChevronUp' }}
                    ></DefaultButton>
                  )}
              </>
            ) : (
              <Label
                onClick={() => {
                  _figma.current.mode = 'node'
                  _figma.main.render()
                }}
                className={`${
                  _figma.current.mode === 'node' ||
                  _figma.current.mode === 'frame'
                    ? 'active'
                    : ''
                }`}
              >
                Root Frame
              </Label>
            )}

            {_figma.current.saving && <Label className="flex">Saving...</Label>}
            {/* <DefaultButton
            css={css`
              margin-left: 2px !important;
              height: 18px !important;
              i {
                font-size: 10px;
                color: #333 !important;
                font-weight: 700 !important;
              }
            `}
            onClick={() => {
              _figma.current.refresh()
            }}
            iconProps={{ iconName: 'Refresh' }}
          ></DefaultButton> */}
          </div>

          <div className="flex flex-row">
            <DefaultButton
              css={css`
                margin-left: 2px !important;
                height: 18px !important;
                i {
                  font-size: 10px !important;
                  color: #333 !important;
                  font-weight: 700 !important;
                }
              `}
              onClick={() => {
                _figma.current.code.triggerFold()
              }}
              iconProps={{ iconName: 'ChevronUnfold10' }}
            ></DefaultButton>
            <Label
              className="flex items-center"
              css={css`
                color: #666 !important;
                font-weight: bold !important;
                font-size: 10px !important;
                font-family: 'Jetbrains Mono', 'SF Mono', Monaco, Menlo,
                  Consolas, 'Ubuntu Mono', 'Liberation Mono', 'DejaVu Sans Mono',
                  'Courier New', monospace !important;
                margin: -1px 0px 1px 0px;
                input {
                  margin-top: -2px;
                }
              `}
            >
              <input
                type="checkbox"
                className="mr-1 leading-none"
                checked={current.code.expandChild}
                onChange={() => {
                  localStorage.setItem(
                    'figma-base-expand-child',
                    !current.code.expandChild ? 'y' : 'n'
                  )
                  current.code.expandChild = !current.code.expandChild
                  _figma.main.relayout()
                }}
              />{' '}
              {` Expand Child`}
            </Label>
          </div>
        </>
      )}
    </div>
  )
}
