import pako from 'pako'
import { BaseWindow } from './window'

declare const window: BaseWindow
const lastCallback = { connect: null, disconnect: null, receive: null }

export const initHmr = () => {
  for (let i of Object.keys(localStorage)) {
    if (
      i.indexOf('csx-') === 0 ||
      i.indexOf('ccx-') === 0 ||
      i.indexOf('dbdef-') === 0
    ) {
      localStorage.removeItem(i)
    }
  }

  window.ws_dev = new WebSocket(
    `ws://${location.hostname}:${location.port}/__hmr`
  ) as any
  window.ws_dev.packAndSend = (msg: any) => {
    window.ws_dev?.send(pako.deflate(JSON.stringify(msg)))
  }

  if (lastCallback && lastCallback.connect) {
    window.ws_dev.onConnected = lastCallback.connect
  }
  if (lastCallback && lastCallback.receive) {
    window.ws_dev.onReceive = lastCallback.receive
  }
  if (lastCallback && lastCallback.disconnect) {
    window.ws_dev.onDisconnected = lastCallback.disconnect
  }

  window.ws_dev.onopen = async () => {
    if (window.ws_dev.onConnected) {
      window.ws_dev.onConnected(window.ws_dev)
    }
  }
  window.ws_dev.onclose = async () => {
    console.log('[HMR] Disconnected, retrying...')

    if (window.ws_dev) {
      if (window.ws_dev.onConnected)
        lastCallback.connect = window.ws_dev.onConnected

      if (window.ws_dev.onReceive)
        lastCallback.receive = window.ws_dev.onReceive

      if (window.ws_dev.onDisconnected)
        lastCallback.disconnect = window.ws_dev.onDisconnected
    }

    if (window.ws_dev.onDisconnected) {
      window.ws_dev.onDisconnected(window.ws_dev)
    }
    if (window.ws_dev.readyState !== 1) {
      setTimeout(() => {
        if (window.ws_dev.readyState !== 1) initHmr()
      }, 1000)
    }
  }
  window.ws_dev.onmessage = async (e) => {
    const msg = JSON.parse(e.data)

    if (window.ws_dev.onReceive) {
      window.ws_dev.onReceive(e, window.ws_dev)
    }

    switch (msg.type) {
      case 'platform-answer':
        {
          let data = undefined
          try {
            if (typeof msg.data === 'string') {
              data = JSON.parse(msg.data)
            }
          } catch (e) {}
          if (window.devAskPlatform) {
            window.devAskPlatform.answers[msg.id || ''](data)
            delete window.devAskPlatform.answers[msg.id || '']
          }
        }
        break
      case 'hmr-pending-reload-all':
        {
          const root = document.getElementById('root')
          root.classList.add('cursor-wait', 'transition-opacity', 'opacity-50')
          document.body.style.background = `linear-gradient(145deg, #f5f5f5 25%, #d8e4fc 25%, #d8e4fc 50%, #f5f5f5 50%, #f5f5f5 75%, #d8e4fc 75%, #d8e4fc 100%)`
          document.body.style.backgroundSize = `69.74px 48.83px`
          console.log('[HMR] Reloading Page')
        }
        break
      case 'hmr-reload-page':
        if (msg.id === window.cms_id) {
          if (window.cms_pages[msg.url]) {
            console.clear()
            window.liveReloadPage()
          }
        } else if (msg.id === window.cms_layout_id) {
          console.clear()
          window.liveReloadLayout()
        }
        break
      case 'hmr-reload-app':
        if (location.pathname !== '/figma') {
          location.reload()
        }
        break
      case 'hmr-reload-all':
        if (location.pathname !== '/dev') {
          location.reload()
        }
        break
      case 'component-reload':
        localStorage[`ccx-${msg.id}`] = msg.html
        window.cms_components[msg.id].loaded = false
        window.liveReloadPage()
        break
    }
  }
}
